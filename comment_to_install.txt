Create translation table
yii migrate --migrationPath=@yii/i18n/migrations/

Rings

Gender - Options - Mens | Women's

Material Type - Options - Gold | Silver | Platinum | Bronze | White Gold


Advanced Filter

Diamond Size - Options - TBC

Carat Weight - Options - TBC

Ring Size - Options - All Possible Ring Sizes

Stone Colour - Options - TBC

Clarity - Options - TBC


--



Earrings

Gender - Options - Mens | Women's

Material Type - Options - Gold | Silver | Platinum | Bronze | White Gold


Advanced Filter

Diamond Size - Options - TBC

Carat Weight - Options - TBC

Ring Size - Options - All Possible Ring Sizes

Stone Colour - Options - TBC

Clarity - Options - TBC


--



Neck Jewellery

Gender - Options - Mens | Women's

Chain Type - Options - Chain | Necklace


Advanced Filter

Material Type - Options - Gold | Silver | Platinum | Bronze | White Gold

Weight - Options - Weight in (g)

Diamonds - Options - Yes | No

Stone Colour - Options - TBC

Clarity - Options - TBC

Chain Length - Options - (cm)


--


Bracelets

Gender - Options - Mens | Women's

Chain Type - Options - Chain | Necklace


Advanced Filter

Material Type - Options - Gold | Silver | Platinum | Bronze | White Gold

Weight - Options - Weight in (g)

Diamonds - Options - Yes | No

Stone Colour - Options - TBC

Clarity - Options - TBC

Chain Length - Options - (cm)


--



Pendants

Gender - Options - Mens | Women's

Chain Type - Options - Chain | Necklace


Advanced Filter

Material Type - Options - Gold | Silver | Platinum | Bronze | White Gold

Weight - Options - Weight in (g)

Diamonds - Options - Yes | No

Stone Colour - Options - TBC

Clarity - Options - TBC

Chain Length - Options - (cm)