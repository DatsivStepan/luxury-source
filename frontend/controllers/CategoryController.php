<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ProductsSearch;
use yii\web\Controller;
use common\models\Categories;
use yii\web\NotFoundHttpException;
use common\models\Products;
use yii\web\Response;
use common\models\Modells;

class CategoryController extends Controller
{
    public function actionView($id)
    {
        $category = $this->findCategory($id);

        $searchModel = new ProductsSearch();
        $searchModel->category_id = $category->id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchAdmin(Yii::$app->request->get('ProductsFilter'));

        return $this->render('index', [
            'category' => $category,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCompare($id)
    {
        $products= $this->findProductsParent($id);
        
        $searchModel = new ProductsSearch();
        $searchModel->parent_id = $products->id;
        //$searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchChild();

        return $this->render('compare', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionGetModellsByBrand()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $res = [];
        $model= null;
        if (Yii::$app->request->post('depdrop_parents')[0]) {
            $model = Modells::getAll(Yii::$app->request->post('depdrop_parents')[0]);
        }

        foreach ($model as $item) {
            $res[] = ['id' => $item->id, 'name' => $item->name];
        }

        return ['output' => $res, 'selected' => ''];
    }
    
    protected function findCategory($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The operation (#' . $id . ') does not exist.');
        }
    }
    
    protected function findProductsParent($id)
    {
        if (($model = Products::find()->where(['id' => $id])->andWhere([
                    "OR",
                    ['parent_id' => 0],
                    ['parent_id' => null],
                ])
                ->andWhere([
                    "OR",
                    ['shop_id' => 0],
                    ['shop_id' => null],
                ])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The operation (#' . $id . ') does not exist.');
        }
    }
}
