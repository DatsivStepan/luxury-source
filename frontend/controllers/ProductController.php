<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ProductsSearch;
use yii\web\Controller;
use common\models\Categories;
use yii\web\NotFoundHttpException;
use common\models\Products;

class ProductController extends Controller
{
    
   public function actionView($id)
    {
        $model = $this->findProduct($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    protected function findProduct($id)
    {
        if ($model = Products::find()->where(['id' => $id])->one()){
//                ->andWhere(['not', ['parent_id' => 0]])
//                ->andWhere(['not', ['parent_id' => null]])
//                ->andWhere(['not', ['shop_id' => null]])
//                ->andWhere(['not', ['shop_id' => null]])
//                        ->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The operation (#' . $id . ') does not exist.');
        }
    }
}

