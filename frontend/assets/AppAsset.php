<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;
/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'fonts/fonts.css',
        'css/plugins/glyphicon.css',
        'css/plugins/jquery-confirm.min.css',
        'css/plugins/slick.css',
        'css/plugins/slick-theme.css',
        'plugins/slick/slick.css',
        'plugins/slick/slick-theme.scss',
        'http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
        'https://use.fontawesome.com/releases/v5.5.0/css/all.css',
        'css/site.css',
        'scss/styles.scss',
        'scss/media.scss',
    ];
    public $js = [
        'js/plugins/jquery-confirm.min.js',
        'plugins/slick/slick.min.js',
        'js/jquery-confirm.settings.js',
        'js/plugins/dropzone.js',
        'js/plugins/slick.min.js',
        'js/main.js',
//        'js/testapi.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    public $jsOptions = array(
        'position' => View::POS_HEAD
    );
}
