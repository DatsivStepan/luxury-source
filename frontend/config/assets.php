<?php

return [
	'app' => [
		'basePath' => '@webroot',
		'baseUrl' => '@web',
        'css' => [
            'scss/site.scss',
            'scss/media.scss',
        ],
        'js' => [

        ],
        'depends' => [
            'yii',
        ],
	],
];
?>
