<?php

namespace frontend\widgets;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use common\models\Products;

class LastProducts extends Widget
{
    public $categoryId;
    public $limit = 10;

    public function init()
    {
    }

    public function run()
    {
        $products = Products::getAllAdmin($this->categoryId, $this->limit);

        return $this->render('last-products', [
            'products' => $products,
        ]);
    }
}