<?php

namespace frontend\widgets;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use common\models\Products;

class SponsoredProducts extends Widget
{
    public $categoryId;
    public $limit = 10;

    public function init()
    {
    }

    public function run()
    {
        $products = \common\models\ProductsSponsored::getAllSponsoredByCategory($this->categoryId, $this->limit);

        return $this->render('sponsored-products', [
            'products' => $products,
        ]);
    }
}