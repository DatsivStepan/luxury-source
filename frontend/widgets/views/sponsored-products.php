<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $posts common\models\Products[] */

?>
<div class="row product-list" style="margin-top: 60px;">
    <?php if (count($products) > 0){ ?>
        <?php foreach ($products as $product) { ?>
            <div class="col-6 col-md-3 pr-it">
                <div class="product-item">
                    <div class="product-image">
                        <img src="<?= $product->getImages(); ?>" alt="<?= $product->name; ?>" class="align-self-center">
                    </div>
                    <div class="product-name sponsores-product-name">
                        <?= $product->name; ?>
                    </div>
                    <div class="product-price sponsores-product-price">
                        From - <?= $product->getPrice(); ?>
                    </div>
                    <div class="product-link">
                        <a href="<?= $product->getLink(); ?>">View</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } else{ ?>
        <div class="col-12 info">
            No product
        </div>
    <?php } ?>
</div>