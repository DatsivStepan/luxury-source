<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="how-works-content">
    <div class="container">
        <div class="section-head"><?= $model->name; ?></div>
        <div class="text-white">
            <?= html_entity_decode($model->content); ?>
        </div>
    </div>
</section>
