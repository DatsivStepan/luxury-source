<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Alert;
/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="user-update">
    <div class="container">
        <div class="section-head">Profile</div>
        <div class="profile-fields">
            <?php if (Yii::$app->session->hasFlash('saved')) { ?>
                <?= Alert::widget([
                    'options' => ['class' => 'alert-success'],
                    'body' => 'Settings saved!',
                ]) ?>
            <?php } ?>
            <?php if (Yii::$app->session->hasFlash('notSaved')) { ?>
                <?= Alert::widget([
                    'options' => ['class' => 'alert-danger'],
                    'body' => 'Settings not saved!',
                ]) ?>
            <?php } ?>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label(false) ?>
            <?= $form->field($model, 'old_password')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('old_password')])->label(false) ?>
            <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('password')])->label(false) ?>
            <?= $form->field($model, 'password_repeat')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('password_repeat')])->label(false) ?>


            <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'update-btn']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</section>