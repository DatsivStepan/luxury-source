<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\Attributes;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use common\models\Brands;
use common\models\CategoriesAttributes;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="panel panel-default">
    <h4 class="refineby-style">REFINE BY</h4>
    <div class="panel-body">
        <?php foreach ($category->getAttributeForFilter(10, CategoriesAttributes::FILTER_TYPE_LEFT) as $attribute) { ?>
            <div class="col-md-12 filter-item">
                <h5 class="refineby-category-name"><div class="refineby-category-triangles"></div><?= $attribute->name; ?></h5>
                <?php foreach ($attribute->getAttributesValueArray() as $aVId => $aValue) { ?>
                <div class="checkbox-style">
                        <?= CheckboxX::widget([
                            'name' => 'ProductsFilter[' . $attribute->id . ']['.$aVId.']',
                            'value' => isset($_GET['ProductsFilter'][$attribute->id][$aVId]) ? $_GET['ProductsFilter'][$attribute->id][$aVId] : null,
                            'options' => [
                                'class' => 'js-filter-left-sidebar',
                                'data-attribute' => $attribute->id,
                                'data-value' => $aVId
                            ],
                            'pluginOptions' => [
                                'threeState' => false,
                                'iconChecked' => '<span class="checked-filter-items"></span>',
                            ]
                        ]); ?>
                        <label class="refineby-category-items"><?= $aValue; ?></label>
                    </div>
                <?php } ?>
            </div>
<!--        <hr style="border-color:white !important;">-->
        <?php }  ?>
    </div>
</div>
<script>
    $(function(){
        $(document).on('change', '.js-filter-left-sidebar', function(){
            var attrId = $(this).data('attribute');
            var attrValId = $(this).data('value');
            var checkbox = $('#product_filter_' + attrId + '_' + attrValId);
            if (checkbox.length) {
                checkbox.val($(this).val())
            }
            $('#form-category-filter').submit()
        })
    })
    $(document).ready(function(){
        $( ".refineby-category-name" ).on( "click", function() {
            $( this ).parent().toggleClass('filter-item-animate')
        });
    });
</script>