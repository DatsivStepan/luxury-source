<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\Attributes;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use common\models\Brands;
use common\models\CategoriesAttributes;
/* @var $this yii\web\View */
/* @var $model frontend\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="panel panel-default">
    <div class="panel-body">
        <?php  $form = ActiveForm::begin([
            'id' => 'form-category-filter',
            'action' => ['/category/' . $category->id],
            'method' => 'get',
        ]); ?>

        <?php if ($category->categoriesAttributeModel) { ?>
            <div class="row">
                <div class="col-md-3 filter-item">
                    <?= $form->field($model, 'brand_id')->widget(Select2::classname(), [
                        'data' => Brands::getAllArrayMapByCategory($category->id),
                        'language' => 'en',
                        'size' => Select2::MEDIUM,
                        'options' => ['placeholder' => 'Select Brand...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false); ?>
                </div>

                <div class="col-md-3 filter-item">
                    <?= $form->field($model, 'modells_id', ['options' => ['class' => 'col-xs-12', 'style' => 'padding: 0']])
                        ->widget(DepDrop::className(), [
                            'type' => DepDrop::TYPE_SELECT2,
                            'options' => ['placeholder' => Yii::t('app', 'Select...')],
                            'select2Options' => [
                                'pluginOptions' => [
                                    'allowClear' => true
                                ]
                            ],
                            'pluginOptions' => [
                                'placeholder' => Yii::t('app', 'Select...'),
                                'initialize' => true,
                                'initDepends' => ['productssearch-brand_id'],
                                'loadingText' => Yii::t('app', 'Select Model...'),
                                'depends' => ['productssearch-brand_id'],
                                'url' => Url::to(['/category/get-modells-by-brand']) . '/' . $model->modells_id,
                            ],
                        ])->label(false) ?>
                </div>

                <?php foreach ($category->getAttributeForFilter(2, CategoriesAttributes::FILTER_TYPE_UP) as $attribute) { ?>
                    <div class="col-md-3 filter-item">
                        <?= Select2::widget([
                            'name' => 'ProductsFilter[' . $attribute->id . ']',
                            'value' => isset($_GET['ProductsFilter'][$attribute->id]) ? $_GET['ProductsFilter'][$attribute->id] : null,
                            'data' => $attribute->getAttributesValueArray(),
                            'options' => ['placeholder' => 'Select a ' . $attribute->name . ' ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);  ?>
                    </div>
                <?php }  ?>
                
            </div>
        <?php } ?>

        <?php foreach ($category->getAttributeForFilter(100, CategoriesAttributes::FILTER_TYPE_LEFT) as $attribute) { ?>
            <?php foreach ($attribute->getAttributesValueArray() as $aVId => $aValue) { ?>
                <?= Html::hiddenInput('ProductsFilter[' . $attribute->id . ']['.$aVId.']', isset($_GET['ProductsFilter'][$attribute->id][$aVId]) ? $_GET['ProductsFilter'][$attribute->id][$aVId] : null, [
                    'id' => 'product_filter_' . $attribute->id . '_' . $aVId
                ]); ?>
            <?php } ?>
        <?php } ?>

        <div class="row">
            <div class="col-md-3 filter-item">
                <?= $form->field($model, 'price_from')->input('number', ['placeholder' => 'Min Price'])->label(false) ?>
            </div>
            <div class="col-md-3 filter-item">
                <?= $form->field($model, 'price_to')->input('number',['placeholder' => 'Max Price'])->label(false) ?>
            </div>
            <div class="col-md-3 filter-item">
                <?= Html::submitButton('<i class="ion-search"></i>', ['class' => 'search-btn']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
