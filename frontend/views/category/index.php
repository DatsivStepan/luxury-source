<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\widgets\SponsoredProducts;
use yii\widgets\LinkPager;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $category->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-category">
    <section class="category-filter">
    <div class="container">
        <div class="post-category-head">Searches for <?= $this->title; ?></div>
        <?= $this->render('_search', ['model' => $searchModel, 'category' => $category]); ?>
    </div>
    </section>
    <section class="sponsored">
        <div class="container">
            <div class="post-category-head text-left">Sponsored Links</div>
            <?= SponsoredProducts::widget(['categoryId' => $category->id, 'limit' => 4]); ?>
        </div>
    </section>
    <section class="newest-listed">
        <div class="container">
            <div class="post-category-head text-left">Newest <?= $this->title; ?> Listed</div>
            <div class="row product-list" style="margin-top: 60px;">
                <div class="col-sm-12 col-md-4 col-lg-3 col-xl-3">
                    <?= $this->render('_left-search', ['model' => $searchModel, 'category' => $category]); ?>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-9 col-xl-9">
                    <div class="row">
                        <?php if (count($dataProvider->getModels()) > 0){ ?>
                            <?php foreach ($dataProvider->getModels() as $product) { ?>
                                <div class="col-12 pr-it">
                                    <div class="row product-item justify-content-center">
                                        <div class="col-8 col-md-6 col-lg-3 col-xl-3">
                                            <div class="product-image">
                                                <img src="<?= $product->getImages(); ?>" alt="<?= $product->name; ?>" class="align-self-center">
                                            </div>
                                        </div>
                                        <div class="col-11 col-md-9 col-lg-7 col-xl-7 product-items-decriptiom">
                                            <div class="product-name">
                                                <h4 style="color:black;"><a href="<?= $product->getLink(); ?>"><?= $product->name; ?></a></h4>
                                            </div>
                                            <div class="product-description" style="color:black;">
                                                <?= $product->description; ?>
                                            </div>
                                        </div>
                                        <div class="col-4 col-md-3 col-lg-2 col-xl-2">
                                            <div class="product-price">
                                                <?= $product->getPrice(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row product-item-other-img">
                                        <?php foreach ($product->productsImagesModel as $image) { ?>
                                            <img src="<?= $image->getImages(); ?>" class="thumbnail-img">
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else{ ?>
                            <div class="col-12 info">
                                No product
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="pagination-pages">
            <?= LinkPager::widget([
                'pagination' => $dataProvider->pagination,
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last',
                'pageCssClass' => 'page',
            ]); ?>
            </div>
        </div>
    </section>
</div>


<?php /*>
 * <div class="col-sm-9">
                    <div class="row">
                        <?php if (count($dataProvider->getModels()) > 0){ ?>
                            <?php foreach ($dataProvider->getModels() as $product) { ?>
                                <div class="col-6 col-md-3 pr-it">
                                    <div class="product-item">
                                        <div class="product-image">
                                            <img src="<?= $product->getImages(); ?>" alt="<?= $product->name; ?>" class="align-self-center">
                                        </div>
                                        <div class="product-name">
                                            <?= $product->name; ?>
                                        </div>
                                        <div class="product-price">
                                            From - <?= $product->getPrice(); ?>
                                        </div>
                                        <div class="product-link">
                                            <a href="<?= $product->getLink(); ?>">Compare prices</a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else{ ?>
                            <div class="col-12 info">
                                No product
                            </div>
                        <?php } ?>
                    </div>
                </div>
 * 
<?php */ ?>
<script>
    $(document).ready(function(){
        if($(window).width() <= 414){
            var height = $('.product-image').width();
        }else if($(window).width() <= 991){
            var height = $('.product-image').width() + 75;
        }else{
            var height = $('.product-image').width() - 79;
        }
        $('.product-image').css({
            height : height 
        });
        $( window ).resize(function() {
             if($(window).width() <= 414){
                var height = $('.product-image').width();
            }else if($(window).width() <= 991){
                var height = $('.product-image').width() + 75;
            }else{
                var height = $('.product-image').width() - 79;
            }
            $('.product-image').css({
                height : height 
            });
        });
    });
</script>