<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\widgets\SponsoredProducts;
use yii\widgets\LinkPager;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Searches for Result';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-compare">
    <section class="compare">
        <div class="container">
            <div class="section-head"><?= $this->title; ?></div>
        </div>
        <div class="container">
            <div class="sub-head">Results</div>
            <div class="row product-list" style="margin-top: 10px;">
                <?php if (count($dataProvider->getModels()) > 0){ ?>
                    <?php foreach ($dataProvider->getModels() as $product) { ?>
                        <div class="col-6 col-md-3 pr-it-1">
                            <div class="product-item">
                                <div class="product-image">
                                <a href="<?= $product->getLink(); ?>" class="align-self-center"><img src="<?= $product->getImages(); ?>" alt="<?= $product->name; ?>" class="align-self-center"></a>
                                </div>
                                <div class="product-name">
                                    <?= $product->name; ?>
                                </div>
                                <div class="product-price">
                                    From - <?= $product->getPrice(); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } else{ ?>
                    <div class="col-12 info">
                        No product
                    </div>
                <?php } ?>
            </div>
            <div class="pagination-pages">
            <?= LinkPager::widget([
                'pagination' => $dataProvider->pagination,
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last',
                'pageCssClass' => 'page',
            ]); ?>
            </div>
        </section>
    </div>
</div>
