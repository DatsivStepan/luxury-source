<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\AuthAsset;

AuthAsset::register($this);

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row no-gutters site-login">
  <div class="col-md-12 col-lg-8 left xs-block">
    <div class="row justify-content-center no-gutters">

    <div class="col-12 logo-img d-sm-none d-md-none d-lg-none d-xl-none">
     <a href="/"> <img src="/images/logo.png " alt="Luxury & Source" class="logo"></a>
    </div>
      <div class="col-12">
        <h1>Welcome back,</h1>
      </div>
      <div class="col-auto auth-box">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <?= $form->field($model, 'username')->textInput(['placeholder' => 'USERNAME'])->label(false) ?>
        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'PASSWORD'])->label(false) ?>

        <div class="form-group">
          <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        <div class="after-form">
          <div class="text">Don't have an account?</div>
          <a href="/site/signup">REGISTER HERE</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-4 right-singin d-none d-sm-block">
  <div class="row no-gutters">
    <div class="col-12 logo-img">
     <a href="/"> <img src="/images/logo.png " alt="Luxury & Source" class="logo"></a>>
    </div>
  </div>
  <div class="row no-gutters">
    <div class="col-12 question-text">Ready to source?</div>
  </div>
  <div class="row no-gutters">
    <div class="col-12 for-user">
      If you don't already have an account and would<br>
      like to start your searching now register
    </div>
  </div>
  <div class="row no-gutters">
    <a class="reidrect-btn d-flex align-items-center justify-content-center" href="/site/signup"><span>REGISTER ACCOUNT</span></a>
  </div>
  </div>
</div>