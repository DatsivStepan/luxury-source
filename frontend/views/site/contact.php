<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Configuration;
$setting = Configuration::getAllInArrapMap();

$this->title = 'Contact us';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="contact">
    <div class="container">
        <div class="section-head"><?= Html::encode($this->title) ?></div>
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')) { ?>
        <?php } ?>
        <div class="contact-phone"><?= array_key_exists('phone', $setting) ? $setting['phone'] : '0843 636 47 97'; ?></div>
        <div class="contact-text">
            Want to get in touch? Simply select the department you want to speak to from the dropdown menu below, fill in your details, and a member of the team will contact you as soon as possible.
        </div>

        <div class="row justify-content-center">
            <div class="col-md-7 contact-form">
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                    <div class="row">
                        <div class="col-6 col-md-6 contact-form-1"><?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => 'Pleace enter your first name'])->label('First name'); ?></div>
                        <div class=" col-6 col-md-6 contact-form-2"><?= $form->field($model, 'surname')->textInput(['autofocus' => true, 'placeholder' => 'Pleace enter your last name'])->label('Last name'); ?></div>
                    </div>
                    <div class="row">
                        <div class=" col-6 col-md-6 contact-form-1"><?= $form->field($model, 'email')->input('email', ['placeholder' => 'Pleace enter your email'])->label('Email'); ?></div>
                        <div class="col-6 ol-md-6 contact-form-2"><?= $form->field($model, 'phone')->input('tel', ['placeholder' => 'Pleace enter your telephone no'])->label('Phone'); ?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 textarea-text"><?= $form->field($model, 'body')->textarea(['rows' => 6, 'placeholder' => 'Pleace type your enquiry'])->label('Enquiry') ?></div>
                    </div>
                    <div class="form-group button-cont">
                        <?= Html::submitButton('Submit', ['class' => 'update-btn', 'name' => 'contact-button']); ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>