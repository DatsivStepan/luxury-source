<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Pages;
use common\models\Configuration;
$setting = Configuration::getAllInArrapMap();
?>

<section class="how-works">
	<div class="container">
		<div class="section-slogan">Want to learn more?</div>
		<a href="/pages/how_it_work" class="works-link">How it works!</a>
		
	</div>
</section>
<footer class="site-footer">
    <div class="container">
            <div class="row">
                <?php foreach (Pages::getAllFromFooter() as $pageC) { ?>
                    <div class="col-6 col-sm 3 col-md-3 col-lg-2 foter-text">
                            <div class="munu-title"><?= $pageC['name']; ?></div>
                            <?php if ($pageC['items']) { ?>
                                <?php foreach ($pageC['items'] as $page) { ?>
                                    <div class="menu-link"><a href="<?= $page->getLink(); ?>"><?= $page->name?></a></div>
                                <?php } ?>
                            <?php } ?>
                    </div>
                <?php } ?>

                    <div class="col-12 col-md-12 col-lg-4 foter-text-2">
                            <div class="munu-title">Stay in touch</div>
                            <div class="menu-link"><a href="">Phone | <?= array_key_exists('phone', $setting) ? $setting['phone'] : '0843 636 47 97'; ?> |
                                    <?= array_key_exists('address', $setting) ? $setting['address'] : ' Mon-Fri 9am-7pm GTM'; ?></a></div>
                            <div class="menu-link"><a href="">Email | <?= array_key_exists('email', $setting) ? $setting['email'] : 'support@luxurysource.com'; ?></a></div>
                            <div class="subscrube-box">
                                    <div class="title">Subscribe to our newsletter and recive updates</div>
                                    <div class="subscribe-form">
                                            <form action="" class="clearfix">
                                                    <input type="text" class="subscribe-input" placeholder="Enter your email address">
                                                    <button class="subscribe-btn">Subscribe</button>
                                            </form>
                                    </div>
                            </div>
                            <div class="links-soc">
                                    <a href="<?= array_key_exists('link_instagram', $setting) ? $setting['link_instagram'] : 'https://www.instagram.com/'; ?>"><i class="fab fa-instagram"></i></a>
                                    <a href="<?= array_key_exists('link_linkedin', $setting) ? $setting['link_linkedin'] : 'https://www.linkedin.com/'; ?>"><i class="fab fa-facebook-f"></i></a>
                                    <a href="<?= array_key_exists('link_twitter', $setting) ? $setting['link_twitter'] : 'https://www.twitter.com/'; ?>"><i class="fab fa-twitter"></i></a>
                            </div>
                    </div>
            </div>
            <div class="footer-bottom">
                    <div class="tramenarks col-sm-8">
                        All trademarks appearing on the Luxury Source site are the property of their respective owners. Unless otherwise explicitly stated, 
                        Luxury Source has no affiliation with the owners of any non Luxury Source products and trademarks.
                    </div>
                    <div class="row justify-content-between">
                            <div class="col-md-4 policy">
                                    <a href="">Terms of service</a>  | 
                                    <a href="">Privacy Policy</a> | 
                                    <a href="">Consigment Policy</a>
                            </div>
                            <div class="col-md-4 copyright">
                                    Copyright &copy; 2018. Luxury Source Limited. All Rights Recerved.<br/>
                                    
                            </div>
                    </div>
            </div>
    </div>
</footer>