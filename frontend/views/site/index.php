<?php
use yii\helpers\Url,
    yii\helpers\Html,
    yii\widgets\ActiveForm,
    kartik\select2\Select2,
    frontend\widgets\LastProducts;

/* @var $this yii\web\View */

$this->title = 'Luxury Sourсes';
?>
<div class="site-index">
    <section class="search-box">
        <div class="container">
            <div class="section-head">Find your ideal jewellery, for the best prices</div>
            <div class="section-slogan">"We source the best luxury so you don't have to."</div>
            
                <?php $form = ActiveForm::begin([
                    'id' => 'select-category-form',
                    'method' => 'get',
                    'action' => Url::to(['category/' . $id])
                ]); ?>
                <div class="row justify-content-center no-gutters">
                    <div class="col-8 col-sm-9 col-md-9 xs-select-block">
                        <?= Select2::widget([
                            'name' => 'category',
                            'value' => $id,
                            'data' => $categories,
                        ]);
                        ?>
                    </div>
                    <div class="col-4 col-sm-3 col-md-3 xs-select-block-2">
                        <?= Html::submitButton('<i class="ion-ios-search"></i>', ['class' => 'search']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            
            <div class="buttons-block">
                <?= Html::a(Yii::t('app', 'SIGN UP'), Url::to(['/site/signup']), [
                    'class' => 'signup-btn'
                ]) ?>

                <?= Html::a(Yii::t('app', 'LEARN MORE'), Url::to(['pages/how_it_work']), [
                    'class' => 'more-btn'
                ]) ?>
            </div>
            <div class="row  justify-content-center scroll-down">
                <div class="col-auto"><i class="ion-chevron-down m-auto d-block"></i></div>
            </div>
        </div>        
    </section>
    <section class="top-picks">
        <div class="container">
            <div class="section-head">Top picks</div>
            <div class="section-slogan">check out the most popular searched watches</div>
            <div class="top-items">
                <div class="row top-item m-0 no-gutters">
                    <div class="col-md-7 item-img left">
                    <div class="home-slider">
                        <div class="item"><img src="/images/Luxury watch_1.png" alt="1"></div>
                        <div class="item"><img src="/images/Luxury watch_2.png" alt="2"></div>
                        <div class="item"><img src="/images/Luxury watch_3.png" alt="3"></div>
                    </div>
                    </div>
                    <div class="col-md-5 item-title">
                        <div class="row no-gutters align-items-center justify-content-center">
                            <div class="col-12 col-sm-8 title-text"><?= $category->text_slide_1; ?></div>
                        </div>
                    </div>
                </div>
                <div class="row top-item m-0 no-gutters">
                    <div class="col-md-5 item-title order-2 order-sm-2 order-md-1 order-lg-1 order-xl-1">
                        <div class="row no-gutters align-items-center justify-content-center">
                            <div class="col-12 col-sm-8 title-text"><?= $category->text_slide_2; ?></div>
                        </div>
                    </div>
                    <div class="col-md-7  order-1 order-sm-1 order-md-2 order-lg-2 order-xl-2 item-img right">
                        <div class="home-slider">
                            <div class="item"><img src="/images/Luxury watch_4.png" alt="4"></div>
                            <div class="item"><img src="/images/Luxury watch_5.png" alt="5"></div>
                            <div class="item"><img src="/images/Luxury watch_6.png" alt="5"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="popular-manufactures">
        <div class="container">
            <div class="section-slogan">Select From Popular Manufactures</div>
            <div class="manufactures-box row">
                <?php foreach($brands as $brand) { ?>
                    <div class="col-6 col-md-3 manufacture-item">
                        <?= Html::a($brand->name, Url::to([$brand->getLink()]), [
                            'class' => ''
                        ]); ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <section class="recent-list">
        <div class="container">
            <div class="section-head">Recent Listing</div>
            <div class="section-slogan">Discover our latest watch listings</div>
            <?= LastProducts::widget(['categoryId' => $id, 'limit' => 4]); ?>
        </div>
    </section>
    <section class="source">
        <div class="container">
            <div class="section-head">The source</div>
            <div class="section-slogan">Check the latest news in the luxury jewellery world</div>
            <div class="source-bg">
                <div class="source-content">
                    <div class="source-content-head">The world</div>
                    <div class="source-content-slogan">a blog by luxury source</div>
                    <div class="source-content-text">how to price youre<br/> pre-owned watch<br/> for resale</div>
                    <a href="" class="source-more">Read more</a>
                </div>
            </div>
        </div>
    </section>
    <section class="seen-in">
        <div class="container">
            <div class="section-head">As seen in</div>
            <div class="section-slogan">places we have featured</div>
            <div class="row">
                <div class="col-12 images-sein-in">
                    <img src="/images/asontv_vogue.png" class="vogue">
                    <img src="/images/asontv_ft.png" class="ft">
                    <img src="/images/asontv_wsj.png" class="wsj">
                    <img src="/images/asontv_bloomberg.png" class="bloomberg">
                    <img src="/images/asontv_forbes.png" class="forbes">

                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function(){
        $(document).on('change', '#select-category-form [name=category]', function(){
            window.location.replace("/index?id=" + $(this).val());
        })
    })
</script>
<?php /* ?>
Rings

Gender - Options - Mens | Women's

Material Type - Options - Gold | Silver | Platinum | Bronze | White Gold


Advanced Filter

Diamond Size - Options - TBC

Carat Weight - Options - TBC

Ring Size - Options - All Possible Ring Sizes

Stone Colour - Options - TBC

Clarity - Options - TBC


--



Earrings

Gender - Options - Mens | Women's

Material Type - Options - Gold | Silver | Platinum | Bronze | White Gold


Advanced Filter

Diamond Size - Options - TBC

Carat Weight - Options - TBC

Ring Size - Options - All Possible Ring Sizes

Stone Colour - Options - TBC

Clarity - Options - TBC


--



Neck Jewellery

Gender - Options - Mens | Women's

Chain Type - Options - Chain | Necklace


Advanced Filter

Material Type - Options - Gold | Silver | Platinum | Bronze | White Gold

Weight - Options - Weight in (g)

Diamonds - Options - Yes | No

Stone Colour - Options - TBC

Clarity - Options - TBC

Chain Length - Options - (cm)


--


Bracelets

Gender - Options - Mens | Women's

Chain Type - Options - Chain | Necklace


Advanced Filter

Material Type - Options - Gold | Silver | Platinum | Bronze | White Gold

Weight - Options - Weight in (g)

Diamonds - Options - Yes | No

Stone Colour - Options - TBC

Clarity - Options - TBC

Chain Length - Options - (cm)


--



Pendants

Gender - Options - Mens | Women's

Chain Type - Options - Chain | Necklace


Advanced Filter

Material Type - Options - Gold | Silver | Platinum | Bronze | White Gold

Weight - Options - Weight in (g)

Diamonds - Options - Yes | No

Stone Colour - Options - TBC

Clarity - Options - TBC

Chain Length - Options - (cm)
<?php */ ?>