<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<header class="site-header">
	<a href="/"><img src="/images/logo.png " alt="Luxury &amp; Source" class="logo"></a>
	<div class="right-btns">
	<?php if (Yii::$app->user->isGuest) { ?>
		
		<?= Html::a(Yii::t('app', 'REGISTER'), Url::to(['site/signup']), [
			'class' => 'signup-btn'
		]) ?>
		<?= Html::a(Yii::t('app', 'SIGN IN'), Url::to(['/site/login']), [
			'class' => 'signin-btn'
		]) ?>
	<?php } else { ?>
		<?= Html::beginForm(['/site/logout'], 'post'); ?>
			<div class="dropdown">
				<button class="account-btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				My Account
				</button>
				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
					<?= Html::a(Yii::t('app', 'Profile'), Url::to(['/profile/update']), [
						'class' => 'dropdown-item'
					]) ?>
					<?= Html::submitButton(
						'Logout',
						['class' => 'dropdown-item']
					); ?>
				</div>
			</div>
		<?= Html::endForm(); ?>
	<?php } ?>
	</div>
</header>