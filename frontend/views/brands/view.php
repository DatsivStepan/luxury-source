<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-brand">
    <div class="container">
        <div class="section-head"><?= $model->name; ?></div>
        <img src="<?= $model->getImages(); ?>" class="brand-image">
        <p class="brand-description">
            <?= $model->description; ?>
        </p>
    </div>
</div>
