<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-product">
    <section class="product">
        <div class="container">
            <div class="row">
                <div class="col-md-7 prod-left">
                    <div class="product-viewer">
                        <div class="row">
                            <div class="order-md-1 order-2 col-auto thumbnail-left">
                                <div class="thumbnail">
                                    <?php if ($model->img_src) { ?>
                                        <div class="slick-slide"><img src="<?= $model->getImages(); ?>" class="thumbnail-img"></div>
                                    <?php } ?>
                                    <?php foreach ($model->productsImagesModel as $image) { ?>
                                        <div class="slick-slide"><img src="<?= $image->getImages(); ?>" class="thumbnail-img"></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="order-md-2 order-1 col-auto images-right">
                                <div class="images">
                                    <?php if ($model->img_src) { ?>
                                        <div class="slick-slide"><img src="<?= $model->getImages(); ?>" class="thumbnail-img"></div>
                                    <?php } ?>
                                    <?php foreach ($model->productsImagesModel as $image) { ?>
                                        <div class="slick-slide"><img src="<?= $image->getImages(); ?>" class="product-img"></div>
                                    <?php } ?>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <div class="row product-btns">
                        <div class="col-md-6 my-col">
                            <button class="seller-btn">Contact Seller</button>
                        </div>
                        <div class="col-md-6 my-col">
                            <button class="product-code">Product code: <?= $model->getProductCode(); ?></button>
                        </div>
                    </div>
                    <div class="product-description">
                        <div class="description-title">About this product</div>
                        <p class="description-content"><?= $model->description; ?></p>
                    </div>
                </div>
                
                <div class="col-md-5">
                    <div>
                    <div class="prod-brand"><?= $model->getBrandName(); ?></div>
                    <div class="prod-name"><?= $model->name; ?></div>
                    <div class="prod-code"><?=  $model->getModelsName(); //$model->getParentProductCode(); ?></div>
                    
                    <div class="prod-price"><?= $model->getPrice(); ?></div>
                    <div class="prod-retail">Retail Price Guide : <?= $model->getPriceRetail(); ?></div>
                    
                    <div class="prod-shop"><?= $model->getShopName(); ?></div>
                    </div>
                        
                    <div class="attr-box">
                        <div class="attr-title">Product Description</div>
                        <div class="attr-list">
                            <?php foreach ($model->getProductsAttributesForView() as $productsAttribute) { ?>
                                <div class="row">
                                    <div class="col-6 col-sm-6 attr-name">
                                        <?= $productsAttribute['name']; ?>
                                    </div>
                                    <div class="col-6 col-sm-6 attr-val">
                                        <?= ($productsAttribute['value'])?$productsAttribute['value']:'-'; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function(){
        if($.fn.slick()){
            $('.images').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.thumbnail',
                swipeToSlide: false,
                touchMove: false,
                swipe: false
            });
            $('.thumbnail').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.images',
                arrows: false,
                vertical: true,
                dots: false,
                focusOnSelect: true,
                draggable: false,
                responsive: [
                    {
                      breakpoint: 768,
                      settings: {
                        vertical: false,
                        draggable: true
                      }
                    }
                ]
            });
        }
    });
</script>