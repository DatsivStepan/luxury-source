<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

class ProductsSearch extends Products
{
    public $price_from;
    public $price_to;

    public function rules()
    {
        return [
            [['category_id', 'brand_id', 'modells_id', 'shop_id', 'parent_id'], 'integer'],
            [['description', 'price_from', 'price_to'], 'string'],
            [['created_at', 'updated_at','images', 'imagesSort'], 'safe'],
            [['name', 'img_src'], 'string', 'max' => 255],
            [['price', 'price_retail'], 'string', 'max' => 50],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchAdmin($get)
    {
        $query = self::find()
                ->alias('product')
                ->joinWith(['productsAttributesModel pr_at'])
                ->where([
                    "OR",
                    ['product.parent_id' => 0],
                    ['product.parent_id' => null],
                ])
                ->andWhere([
                    "OR",
                    ['product.shop_id' => 0],
                    ['product.shop_id' => null],
                ])
                ->distinct();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if ($get) {
            foreach ($get as $attributeId => $value) {
                if (is_array($value)) {
                    $newValue = [];
                    foreach ($value as $key => $v) {
                        if ($v) {
                            $newValue[] = $key;
                        }
                    }
                    $value = $newValue;
                }
                if ($value) {
                    $query->andFilterWhere([
                        'AND',
                        ['pr_at.attribute_id' => $attributeId],
                        ['pr_at.value' => $value],
                    ]);
                }
            }
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->price_from || $this->price_to) {
            if ($this->price_from > $this->price_to) {
                $price_to = $this->price_from;
                $this->price_from = $this->price_to;
                $this->price_to = $price_to;
            }
            $query->andFilterWhere(['between', 'product.price', (int)$this->price_from, (int)$this->price_to]);
        }

//        foreach($get as $attributeId) {
//            $query->andFilterWhere([
//
//            ]);
//        }

        $query->andFilterWhere([
            'product.id' => $this->id,
            'product.category_id' => $this->category_id,
            'product.modells_id' => $this->modells_id,
            'product.brand_id' => $this->brand_id,
        ]);

        //$query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
    
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchShop()
    {
        $query = self::find()
                ->alias('product')
                ->where(['shop_id' => $this->shop_id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        //$query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchChild()
    {
        $query = self::find()
                ->alias('product')
                ->where(['parent_id' => $this->parent_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 12
            ]
        ]);


        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        //$query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
