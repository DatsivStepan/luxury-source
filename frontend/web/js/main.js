$(document).ready(function(){
  if ($.fn.slick) {
    $('.home-slider').slick({
      arrows: false,
      autoplay: true,
      autoplaySpeed: 3000,
      pauseOnFocus: false,
    });
  }
});