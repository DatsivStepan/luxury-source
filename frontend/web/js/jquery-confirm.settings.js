var $modal = {};
var $modalUrl = {};

function modalIFrameHeight(id, addHeight) {
    id = id || '#modal-iframe';
    addHeight = addHeight || 0;
    $('#iframe-first').remove();
    var height = $(id).contents().height() + addHeight;
    setTimeout(function () {
        $(id).animate({opacity: 1}, 400);
    }, 0);
    $(id).animate({height: height}, 200);
}

function modalIFrame(urlIframe) {
    return '<div id="iframe-first" style="position: absolute; opacity: 0.5;"><img height="24px" src="/img/loader-big.gif"></div>' +
        '<iframe id="modal-iframe" src="' + urlIframe + '" width="100%" height="250px" onload="modalIFrameHeight(this)" frameborder="0" style="opacity: 0;">';
}

function modalReload(modal, url, func) {
    func = func || 0;
    url = url || 0;
    if (!func) {
        func = function(html) {
            modal.setContent(html);
        };
    }
    if (!url) {
        var id = modal.id;
        url = $modalUrl[id];
    }
    clearPjaxSubmits();
    if (typeof modal === 'object') {
        modal.$content.empty();
    }
    $.get(url, func);
}

function clearPjaxSubmits() {
    $(document).find('div[id^="pjax"][data-pjax-container]').each(function(index) {
        $(document).off("submit", "#" + $(this).attr('id') + " form[data-pjax]");
    });
}

function modalSubmitButton($this, $button) {
    if ($this.modal === 'alert') {
        return true;
    }

    var $form = $('.jconfirm-open:last').find('form:first');
    $button.prop({disabled: true});
    $form.submit();
    setTimeout(function () {
        $button.prop({disabled: false});
    }, 2000);

    return false;
}

function needOnClose($this) {
    setTimeout(function () {
        if (!$('.jconfirm-open').length) {
            $('body').removeClass('overflow_hidden');
        }
    }, 100);

    if ($this.id !== undefined && $this.id) {
        var id = $this.id;
        if ($modal[id] !== undefined) {
            delete $modal[id];
        }
    }

    clearPjaxSubmits();
}

jconfirm.defaults = {
    typeAnimated: true,
    draggable: true,
    columnClass: 'l',
    title: '',
    icon: '',
    content: ' ',
    lazyOpen: false,
    bgOpacity: 0.65,
    animation: 'scale',
    backgroundDismiss: true,
    closeIcon: true,
    scrollToPreviousElement: false,
    defaultButtons: {
        close: {
            text: 'Anuluj',
            btnClass: 'btn-default'
        },
        ok: {
            text: 'Zapisz',
            btnClass: 'btn-primary',
            action: function(saveButton) {
                return modalSubmitButton(this, this.$$ok);
            }
        }
    },
    buttons: {
        close: {
            text: 'Anuluj',
            btnClass: 'btn-default'
        },
        ok: {
            action: function (saveButton) {
                return modalSubmitButton(this, this.$$ok);
            }
        }
    },
    onOpenBefore: function () {
        $('body').addClass('overflow_hidden');

        if (this.id !== undefined && this.id) {
            var id = this.id;
            $modal[id] = this;
            var content = this.content.toString();
            if (content.indexOf('url:') !== -1) {
                var u = this.content.split('url:');
                $modalUrl[id] = u[1];
            }
        }

        var modal = this.modal !== undefined ? this.modal : false;
        if ((this.buttonsHide !== undefined && this.buttonsHide) || (modal === 'dialog')) {
            this.$btnc.remove();
        }

        if (!this.closeIcon) {
            this.buttons.close.hide();
        }

        if (modal) {
            if (modal === 'alert') {
                if (this.type === 'default') {
                    this.setType('red');
                }
                this.setColumnClass('m');
                this.buttons.close.hide();
                this.buttons.ok.setText('Ok');
            }
            if (modal === 'confirm') {
                if (this.type === 'default') {
                    this.setType('blue');
                }
            }
            if (modal === 'dialog') {
                if (this.type === 'default') {
                    this.setType('purple');
                }
            }
        }
    }
};