<?php

namespace shop\controllers;

use Yii;
use common\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Shops;
use frontend\models\ProductsSearch;
use common\models\Products;
use common\models\Categories;
use yii\web\Response;
use common\models\ProductsAttributes;
use common\models\ProductsImages;

class ProfileController extends Controller
{
    public $shop_id;
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        if (\Yii::$app->user->isGuest){
            throw new \yii\web\NotFoundHttpException();
        }
        $this->shop_id = Yii::$app->user->identity->shop_id;
        
        return $this;
    }
    
    public function actionIndex()
    {
        $model = $this->findModel();
        $model->scenario = 'shop-update';
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionGetProductsByCategory()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $res = [];
        $model = Products::getAllAdmin(Yii::$app->request->post('depdrop_parents')[0]);

        foreach ($model as $item) {
            $res[] = ['id' => $item->id, 'name' => $item->name];
        }

        return ['output' => $res, 'selected' => ''];
    }
    
    public function actionAddProduct()
    {
        $this->layout = 'modal';
        $categories = Categories::getAllInArrayMap();
        $productNew = new Products();
        if (Yii::$app->request->isPost && $productNew->load(Yii::$app->request->post())) {
            $parent = Products::findOne(['id' => $productNew->parent_id]);
            $productNew->name = $parent->name;
            $productNew->shop_id = $this->shop_id;
            $productNew->brand_id = $parent->brand_id;
            if ($productNew->save()) {
                if (Yii::$app->request->post('ProductsAttributes')) {
                    ProductsAttributes::saveProductAttributes(Yii::$app->request->post('ProductsAttributes'), $productNew->id);
                }
                if (Yii::$app->request->post('Products')['images']) {
                    ProductsImages::saveProductImages(
                        Yii::$app->request->post('Products')['images'],
                        Yii::$app->request->post('Products')['imagesSort'],
                        $productNew->id
                    );
                }
                Yii::$app->session->setFlash('saveData');
            }
        }
        
        return $this->render('add-product', [
            'categories' => $categories,
            'productNew' => $productNew,
        ]);
    }
    
    public function actionUpdateProduct($id)
    {
        $this->layout = 'modal';
        $categories = Categories::getAllInArrayMap();
        $productNew = $this->findProductModel($id);
        
        $modelCategoryAttribute = $productNew->category_id ? 
                \common\models\CategoriesAttributes::find()
                        ->where(['category_id' => $productNew->category_id])
                        ->all() : null;
        $modelProductsAttributes = $productNew->category_id ? 
                \yii\helpers\ArrayHelper::map(ProductsAttributes::find()
                        ->where(['product_id' => $productNew->id])
                        ->all(), 'attribute_id', 'value') : null;
        
        if (Yii::$app->request->isPost && $productNew->load(Yii::$app->request->post())) {
            $parent = Products::findOne(['id' => $productNew->parent_id]);
            $productNew->name = $parent->name;
            $productNew->shop_id = $this->shop_id;
            $productNew->brand_id = $parent->brand_id;
            if ($productNew->save()) {
                if (Yii::$app->request->post('ProductsAttributes')) {
                    ProductsAttributes::saveProductAttributes(Yii::$app->request->post('ProductsAttributes'), $productNew->id);
                }
                if (Yii::$app->request->post('Products')['images']) {
                    ProductsImages::saveProductImages(
                        Yii::$app->request->post('Products')['images'],
                        Yii::$app->request->post('Products')['imagesSort'],
                        $productNew->id
                    );
                }
                Yii::$app->session->setFlash('saveData');
            }
        }
        
        return $this->render('update-product', [
            'categories' => $categories,
            'productNew' => $productNew,
            'modelProductsAttributes' => $modelProductsAttributes,
            'modelCategoryAttribute' => $modelCategoryAttribute,
        ]);
    }

    
    
    public function actionDeleteProduct($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['status' => $this->findProductModel($id)->delete()];
    }
    
    
    public function actionProducts()
    {
        $model = $this->findShopModel();

        $modelNewProducts = new Products;
        $searchModel = new ProductsSearch();
        $searchModel->shop_id = $model->id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchShop();

        return $this->render('index', [
            'modelNewProducts' => $modelNewProducts,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findModel()
    {
        if (($model = User::findOne(Yii::$app->user->id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function findProductModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findShopModel()
    {
        if (($model = Shops::findOne(Yii::$app->user->identity->shop_id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
