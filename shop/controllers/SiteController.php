<?php
namespace shop\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\SignupForm;
use common\models\User;
use common\models\Shops;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'signup', 'index'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        if (\Yii::$app->user->isGuest) {
            return $this->redirect('/shop/login');
        } else {
            return $this->redirect('/shop/profile');
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'main-without';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->loginShop()) {
            return $this->redirect('/shop/profile/');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionSignup()
    {
        $this->layout = 'main-without';

        $model = new User();
        $model->scenario = 'shop';
        if ($model->load(Yii::$app->request->post())) {
            $model->type = User::USER_TYPE_SHOP;

            if ($user = $model->signup()) {
                $modelShops = new Shops();
                $modelShops->name = $model->company_name;
                if ($modelShops->save()) {
                    $user->shop_id = $modelShops->id;
                    $user->save();
                }

                if (Yii::$app->getUser()->login($user)) {
                    return $this->goBack();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
