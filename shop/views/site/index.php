<?php
use frontend\widgets\HomeCategories;

/* @var $this yii\web\View */
$this->registerJsFile('js/home.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = 'My Yii Application';
?>
    <section class="search">
        <div class="container">
            <div class="row">
                <div class="col-lg-6"></div>
                <div class="col-lg-6"></div>
            </div>
        </div>
    </section>
    <section class="howWorks">
        <div class="container">
            <div class="sectionTitle">How Exkavate works<br></div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 work-block find">
                    <div class="work-block-title">Find</div>
                    <div class="work-block-text">
                        The largest selection of ads from your favorite newspapers and magazines are right here
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 work-block save">
                    <div class="work-block-title">Save</div>
                    <div class="work-block-text">
                        Save ads on your Exkavate account or computer in high resolution
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 work-block share">
                    <div class="work-block-title">Share</div>
                    <div class="work-block-text">
                        Broadcast the best deals, job opportunities, civic notices, events and more to family, colleagues and friends
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 work-block subscribe">
                    <div class="work-block-title">Subscribe</div>
                    <div class="work-block-text">
                        Stay up to date when you sign up on Exkavate to receive adverts of interest
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?=HomeCategories::widget();?>
    
    