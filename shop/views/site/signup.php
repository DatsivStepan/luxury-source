<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\AuthAsset;

AuthAsset::register($this);

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row no-gutters site-signup">
  <div class="col-md-12 col-lg-4 left-signup d-none d-sm-block">
    <div class="row no-gutters">
      <div class="col-12 logo-img">
     <a href="/"> <img src="/images/logo.png " alt="Luxury & Source" class="logo"></a>
      </div>
    </div>
    <div class="row no-gutters">
      <div class="col-12 question-text">One of us?</div>
    </div>
    <div class="row no-gutters">
      <div class="col-12 for-user">
        If you already have an account <br>  
        just sign in. We've missed you
      </div>
    </div>
    <div class="row no-gutters">
      <a class="reidrect-btn d-flex align-items-center justify-content-center" href="/shop/login"><span>SIGN IN</span></a>
    </div>
  </div>
  <div class="col-md-12 col-lg-8 left ">
    <div class="row no-gutters d-block d-sm-none">
      <div class="col-12 logo-img ">
      <a href="/"><img src="/images/logo2.png " alt="Luxury & Source" class="logo"></a>
      </div>
    </div>
    <div class="row justify-content-center no-gutters">
      <div class="col-12">
        <h1>Time to feel at home,</h1>
      </div>
      <div class="col-auto auth-box">
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
          <?= $form->field($model, 'company_name')->textInput(['placeholder' => 'SHOP NAME'])->label(false) ?>
          <?= $form->field($model, 'email')->input('email', ['placeholder' => 'EMAIL'])->label(false) ?>
          <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'PASSWORD'])->label(false) ?>
          <?= $form->field($model, 'password_repeat')->passwordInput(['placeholder' => 'CONFIRM PASSWORD'])->label(false) ?>
          <div class="form-group">
            <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
          </div>
        <?php ActiveForm::end(); ?>
        <div class="after-form">
          <div class="text">Don't have an account?</div>
          <a href="/shop/login">LOG IN HERE</a>
        </div>
      </div>
    </div>
  </div>
</div>