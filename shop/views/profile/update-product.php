<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Alert;
use kartik\checkbox\CheckboxX;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use common\models\Attributes;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\User */
$uid = uniqid();
$productArray = yii\helpers\ArrayHelper::map(common\models\Products::getAllAdmin($productNew->category_id), 'id', 'name');
?>
<?php Pjax::begin(['id' => 'pjax-form' . $uid, 'enablePushState' => false]); ?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>
<?php if (Yii::$app->session->hasFlash('saveData')) { ?>
    <script>
        parent.$modal.addProductModal.close();
        parent.$('#product-filter').submit()
    </script>
<?php } ?>

  <div class=" settings-page">
    <?php $form = ActiveForm::begin(['id' => 'product-form', 'options' => ['data-pjax' => 'pjax-form' . $uid]]); ?>

        <?= $form->field($productNew, 'category_id')->widget(Select2::classname(), [
            'data' => $categories,
            'language' => 'en',
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Select ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($productNew, 'parent_id')->widget(DepDrop::className(), [
                'type' => DepDrop:: TYPE_SELECT2,
                'data' => $productArray,
                'options' => ['placeholder' => Yii::t('app', 'Select...')],
                'select2Options' => [
                    'pluginOptions' => [
                        'allowClear' => true
                    ],

                ],
                'pluginEvents' => [
                    'change' => "function() {
                        if($(this).val()){
                            $('.js-all-product-info').show();
                        } else {
                            $('.js-all-product-info').hide();
                        }
                    }",
                    'init' => "function() {
                        if($(this).val()){
                            $('.js-all-product-info').show();
                        } else {
                            $('.js-all-product-info').hide();
                        }
                    }"
                ],
                'pluginOptions' => [
                    'placeholder' => Yii::t('app', 'Select...'),
                    'depends' => ['products-category_id'],
                    'url' => Url::to(['/profile/get-products-by-category']),
                ],
            ]) ?>
      
      <div class="js-all-product-info" style="display:<?= array_key_exists($productNew->parent_id, $productArray) ? 'block' : 'none'; ?>;">
          
            <div style="width:150px;height:150px;">
                <?= $form->field($productNew, 'img_src')->hiddeninput()->label(false); ?>
                <img src="<?= $productNew->getImages(); ?>" class="previewProductImage img-thumbnail" id="js-add-product-image"><br>
                <a class="btn btn-sm btn-success addProductImage" style="width:100%">Change image</a><br><br>
            </div>

            <div class="clearfix"></div>
            <div class="card">
                <h4 class="card-header">Products attributes</h4>
                <div class="card-block" style="padding:15px;">
                    <div class="js-products-options">
                        <?php if ($modelCategoryAttribute) { ?>
                            <?php foreach($modelCategoryAttribute as $categoryAttribute) { ?>
                                <div>
                                    <?php $attribute = $categoryAttribute->attributesModel ? $categoryAttribute->attributesModel : null; ?>
                                    <?php if(!$attribute) { continue; } ?>
                                    <h4><?= $attribute->name;?></h4>
                                    <?php 
                                        $attributeValueArray = $attribute->attributesValueModel ? $attribute->getAttributesValueArray() : [];
                                        $value = $modelProductsAttributes ? $modelProductsAttributes[$attribute->id] : null;

                                        switch ($attribute->type) {
                                            case Attributes::TYPE_TEXT:
                                                echo Html::textInput('ProductsAttributes[' . $attribute->id . ']', $value, ['class' => 'form-control']);
                                                break;
                                            case Attributes::TYPE_SELECT:
                                                echo Html::dropDownList('ProductsAttributes[' . $attribute->id . ']', $value, $attributeValueArray, ['class' => 'form-control', 'prompt' => 'Select...']);
                                                break;
                                            case Attributes::TYPE_CHECKBOX:
                                                echo Html::checkboxList('ProductsAttributes[' . $attribute->id . ']', json_decode($value), $attributeValueArray);
                                                break;
                                            case Attributes::TYPE_RADIO:
                                                echo Html::radioList('ProductsAttributes[' . $attribute->id . ']', $value, $attributeValueArray);
                                                break;
                                        }
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <hr>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>


            <hr>

            <div class="card">
                <h4 class="card-header">Products Images</h4>
                <div class="card-block" style="padding:15px;">
                    
                    
                    <!-- Пустий але потрібний мені блок-->
                    <div style="display:none;">
                        <div class="row-table-style">
                            <div class="table table-striped" class="files" id="prev">
                                <div id="temp" class="file-row">

                                </div>
                            </div>
                        </div>
                    </div> 
                    <!-- Пустий але потрібний мені блок-->
                    
                        <table class="table table-striped table-bordered table-hover js-table-images">
                            <tbody class="container-item">
                                <?php if ($productNew->productsImagesModel) { ?>
                                    <?php foreach ($productNew->productsImagesModel as $key => $productsImages) { ?>
                                        <tr class="js-item">
                                            <td style="max-width: 200px;vertical-align: middle;">
                                                <div style="width:150px;height:150px;">
                                                    <?= $form->field($productNew, 'images[]')->hiddeninput(['value' => $productsImages->img_src])->label(false); ?>
                                                    <img src="/<?= $productsImages->img_src; ?>" class=" img-thumbnail"><br>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <?= $form->field(
                                                    $productNew,
                                                    'imagesSort[]',
                                                    [
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'value' => $productsImages->sort,
                                                        ]
                                                    ]
                                                )->textInput() ?>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <?= Html::a(
                                                    Html::tag('i', '-', ['class' => 'glyphicon glyphicon-minus']),
                                                        null,
                                                    ['class' => 'js-remove-item btn btn-sm btn-danger btn-sm']
                                                ) ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td class="text-right">
                                        <?= Html::a(
                                            Html::tag('i', '+', ['class' => 'glyphicon glyphicon-plus']),
                                                null,
                                            ['class' => 'addProductImages add-item btn btn-sm btn-default btn-sm pull-right']
                                        ) ?>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div style="padding:10px;">

                <div class="form-group row">
                    <div class="col-sm-6">
                        <?= $form->field($productNew, 'price')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-sm-6">
                        <?= $form->field($productNew, 'price_retail')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <?= $form->field($productNew, 'description')->textarea(['rows' => 3]) ?>

            </div>
            <div class="form-group">
                <?= Html::submitButton($productNew->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $productNew->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

  </div>
<script>
    $(function(){
        
         $(document).on('click', '.js-remove-item', function(){
            $(this).closest('tr.js-item').remove()
        })
        
        $(document).on('change', '#products-category_id', function(){
            var category_id = $(this).val();
            $('.js-products-options').html('')
            if (category_id) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/products/products/get-category-attribute?id=' + category_id,
                    dataType:'json',
                    success: function(response){
                        if (response.status) {
                            $('.js-products-options').html('')
                            var attributes = response.data;
                            $.each( attributes, function( attribute_id, attribute ) {
//                                console.log( attribute)
                                switch (attribute['type']) {
                                    case <?= Attributes::TYPE_TEXT; ?>:
                                        $('.js-products-options').append(
                                            $('<div/>').append(
                                                $('<h5/>', {
                                                    text:attribute['name']
                                                }),
                                                $('<input/>', {
                                                    type: 'text',
                                                    class: 'form-control',
                                                    name: 'ProductsAttributes['+attribute_id+']',
                                                }),
                                                $('<div/>', {
                                                    class:'clearfix'
                                                }),
                                            ),
                                            $('<hr/>')
                                        )
                                        break;
                                    case <?= Attributes::TYPE_SELECT; ?>:
                                        $('.js-products-options').append(
                                            $('<div/>').append(
                                                $('<h5/>', {
                                                    text:attribute['name']
                                                }),
                                                $('<select/>', {
                                                    type: 'text',
                                                    class: 'form-control js-products-select-attributes-' + attribute_id,
                                                    name: 'ProductsAttributes['+attribute_id+']',
                                                }),
                                                $('<div/>', {
                                                    class:'clearfix'
                                                }),
                                            ),
                                            $('<hr/>')
                                        )
                                
                                        if(attribute['attributeValue']) {
                                            $('.js-products-select-attributes-' + attribute_id).append(
                                                $('<option/>', {
                                                    value:'',
                                                    text:'Select...'
                                                })
                                            )
                                            $.each( attribute['attributeValue'], function( key, attributeValue ) {
                                                $('.js-products-select-attributes-' + attribute_id).append(
                                                    $('<option/>', {
                                                        value:attributeValue['id'],
                                                        text:attributeValue['value']
                                                    })
                                                )
                                            })
                                        }
                                        break;
                                    case <?= Attributes::TYPE_CHECKBOX; ?>:
                                                
                                        $('.js-products-options').append(
                                            $('<div/>').append(
                                                $('<h5/>', {
                                                    text:attribute['name']
                                                }),
                                                $('<div/>',{
                                                    class:'js-products-checkbox-attributes-' + attribute_id
                                                })
                                            ),
                                            $('<hr/>')
                                        )
                                        $.each( attribute['attributeValue'], function( key, attributeValue ) {
                                            $('.js-products-checkbox-attributes-' + attribute_id).append(
                                                $('<label/>',{
                                                    text:attributeValue['value']
                                                }).prepend(
                                                    $('<input/>', {
                                                        type: 'checkbox',
                                                        name: 'ProductsAttributes['+attribute_id+'][]',
                                                        value: attributeValue['id'],
                                                    }),
                                                )
                                            )
                                        })
                                        break;
                                    case <?= Attributes::TYPE_RADIO; ?>:
                                        $('.js-products-options').append(
                                            $('<div/>').append(
                                                $('<h5/>', {
                                                    text:attribute['name']
                                                }),
                                                $('<div/>',{
                                                    class:'js-products-radio-attributes-' + attribute_id
                                                })
                                            ),
                                            $('<hr/>')
                                        )
                                        $.each( attribute['attributeValue'], function( key, attributeValue ) {
                                            $('.js-products-radio-attributes-' + attribute_id).append(
                                                $('<label/>',{
                                                    text:attributeValue['value']
                                                }).prepend(
                                                    $('<input/>', {
                                                        type: 'radio',
                                                        name: 'ProductsAttributes['+attribute_id+']',
                                                        value: attributeValue['id'],
                                                    }),
                                                )
                                            )
                                        })
                                        break;
                                }
                            });
                        }
                    }
                })
            }
        })
        
        if($('.addProductImage').length){
            var previewNode = document.querySelector("#template");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzone = new Dropzone('#js-add-product-image', {
                url: "/admin/products/products/save-photo",
                previewTemplate: previewTemplate,
                previewsContainer: "#previews",
                clickable: ".addProductImage",
                uploadMultiple:false,
                maxFiles:1,
            });

            myDropzone.on("maxfilesexceeded", function(file) {
                    myDropzone.removeAllFiles();
                    myDropzone.addFile(file);
            });

            myDropzone.on("complete", function(response) {
                if (response.status == 'success') {
                    $('.previewProductImage').attr('src', '/'+response.xhr.response)
                    $('#products-img_src').val(response.xhr.response);
                }
            });

            myDropzone.on("removedfile", function(response) {
                if(response.xhr != null){
                   //deleteFile(response.xhr.response);
                }
            });

        }
        
        
        if($('.addProductImages').length){
            var previewNode = document.querySelector("#temp");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzoneI = new Dropzone('#product-form', {
                url: "../../../admin/products/products/save-photo",
                previewTemplate: previewTemplate,
                previewsContainer: "#prev",
                clickable: ".addProductImages",
                uploadMultiple:false,
                maxFiles:1,
            });

            myDropzoneI.on("maxfilesexceeded", function(file) {
                    myDropzoneI.removeAllFiles();
                    myDropzoneI.addFile(file);
            });

            myDropzoneI.on("complete", function(response) {
                if (response.status == 'success') {
                    $('.js-table-images .container-item').append(
                        $('<tr/>', {
                            class:'item'
                        }).append(
                            $('<td/>', {
                                style:'max-width: 200px;vertical-align: middle;'
                            }).append(
                                $('<div/>', {
                                    style:'width:150px;height:150px;'
                                }).append(
                                    $('<input/>', {
                                        type:'hidden',
                                        id:'products-imgages',
                                        name:'Products[images][]',
                                        class:'form-control',
                                        value:response.xhr.response,
                                    }),
                                    $('<img/>', {
                                        src:'/'+response.xhr.response,
                                        class:'img-thumbnail',
                                    }),
                                )
                            ),
                            $('<td/>', {
                                style:'vertical-align: middle;'
                            }).append(
                                $('<div/>', {
                                    class:'form-group field-products-images_sort has-success'
                                }).append(
                                    $('<label/>', {
                                        class:'control-label',
                                        for:'products-imagesSort',
                                        text:'Sort',
                                    }),
                                    $('<input/>', {
                                        type:'text',
                                        id:'products-imagesSort',
                                        class:'form-control',
                                        name:'Products[imagesSort][]',
                                    }),
                                )
                            ),
                            $('<td/>', {
                                style:'vertical-align: middle;'
                            }).append(
                                $('<a/>', {
                                    class:'remove-item btn btn-sm btn-danger btn-sm'
                                }).append(
                                    $('<i/>', {
                                        class:'glyphicon glyphicon-minus'
                                    })
                                )
                            )
                        )
                    )
    //                $('.previewProductImage').attr('src', '/'+response.xhr.response)
    //                $('#products-img_src').val(response.xhr.response);
                }
            });

            myDropzoneI.on("removedfile", function(response) {
                if(response.xhr != null){
                   //deleteFile(response.xhr.response);
                }
            });

        }
    })
</script>
<?php Pjax::end(); ?>