<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\widgets\SponsoredProducts;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use common\models\Attributes;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $category->name;
$this->params['breadcrumbs'][] = $this->title;
$uid = uniqid();
?>
<section class="shop-update">
    <div class="container">
        <div class="post-index">
            <ul class="nav nav-tabs shop-tabs">
                <li class="nav-item">
                <a class="nav-link" href="<?= Url::to(['/profile/']); ?>">Update</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="#">Products</a>
                </li>
            </ul>
            <div class="text-right">
                <button class="add-product js-add-product">Add product</button>
            </div>
            
            <?php Pjax::begin(['id' => 'pjax-form' . $uid, 'enablePushState' => false]); ?>
                <?php $form = ActiveForm::begin(['id' => 'product-filter', 'options' => ['data-pjax' => 'pjax-form' . $uid]]); ?>
                <?php ActiveForm::end(); ?>
                <div class="container">
                    <div class="post-category-head text-left">Newest Listed</div>
                    <div class="row product-list" style="margin-top: 10px;">
                        <?php if (count($dataProvider->getModels()) > 0){ ?>
                            <?php foreach ($dataProvider->getModels() as $product) { ?>
                                <div class="col-6 col-md-3">
                                    <div class="product-item">
                                        <div class="product-image">
                                        <a href="<?= $product->getLink(); ?>" class="align-self-center"><img src="<?= $product->getImages(); ?>" alt="<?= $product->name; ?>" class="align-self-center"></a>
                                        </div>
                                        <div class="product-name">
                                            <?= $product->name; ?>
                                        </div>
                                        <div class="product-price">
                                            From - <?= $product->getPrice(); ?>
                                        </div>
                                        <button data-product_id="<?= $product->id; ?>" class="add-product js-update-product" style="width:100%;margin-top:5px;">Update product</button>
                                        <button data-product_id="<?= $product->id; ?>" class="add-product js-delete-product" style="width:100%;margin-top:10px;background-color: transparent;border:1px solid white;">Delete product</button>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else{ ?>
                            <div class="col-12 info">
                                No product
                            </div>
                        <?php } ?>
                    </div>
                    <div class="pagination-pages">
                    <?= LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'pageCssClass' => 'page',
                    ]); ?>
                    </div>
                </div>
            <?php Pjax::end(); ?>
        </div>
    </div>
</section>
<script>
    $(function(){
        $(document).on('click', '.js-add-product', function(){
            $.dialog({
                id: 'addProductModal',
                title: 'AddProduct',
                content: 'url:<?= Url::to(['/profile/add-product/']) ?>',
                containerFluid: true,
            });
        })

        $(document).on('click', '.js-delete-product', function(){
            var product_id = $(this).data('product_id')
            $.ajax({
                type: 'POST',
                url:'<?= Url::to(['/profile/delete-product/']) ?>/?id=' + product_id,
                dataType:'json',
                success: function(response){
                    if (response.status) {
                        $('#product-filter').submit()
                    }
                }
            })
        })

        $(document).on('click', '.js-update-product', function(){
            $.dialog({
                id: 'addProductModal',
                title: 'AddProduct',
                content: 'url:<?= Url::to(['/profile/update-product/']) ?>/?id=' + $(this).data('product_id'),
                containerFluid: true,
            });
        })
    })
</script>