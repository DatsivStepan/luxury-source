<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Alert;
use kartik\checkbox\CheckboxX;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['users/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
 
$shopName = $model->shopsModel ? $model->shopsModel->name : ''; 
?>
<section class="shop-update">
	<div class="container">
		<div class="row settings-page">
			<div class="col-md-12">
				<ul class="nav nav-tabs shop-tabs">
					<li class="nav-item">
					<a class="nav-link active" href="#">Update</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= Url::to(['/profile/products']); ?>">Products</a>
					</li>
				</ul>
				<div class="section-head"><?= Html::encode($this->title) ?></div>
				<div class="shop-fields">
					<?php $form = ActiveForm::begin(); ?>
					<?php if (Yii::$app->session->hasFlash('dataSave')) { ?>
					<?= Alert::widget([
						'options' => ['class' => 'alert-success'],
						'body' => 'Updated!',
						]) ?>
					<?php } ?>

					<?= $form->field($model, 'company_name')->textInput(['value' => $shopName]) ->label(true) ?>

					<?= $form->field($model, 'email')->input('email') ->label(true) ?>

					<?php if (Yii::$app->session->hasFlash('passwordSave')) { ?>
					<?= Alert::widget([
						'options' => ['class' => 'alert-success'],
						'body' => 'Password changed!',
					]) ?>
					<?php } ?>

					<?php if (Yii::$app->session->hasFlash('passwordNotSave')) { ?>
						<?= Alert::widget([
						'options' => ['class' => 'alert-danger'],
						'body' => 'Old password is wrong!',
						]) ?>
					<?php } ?>
					<?= $form->field($model, 'old_password')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('old_password')])->label(false) ?>
					<?= $form->field($model, 'password')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('password')])->label(false) ?>
					<?= $form->field($model, 'password_repeat')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('password_repeat')])->label(false) ?>
					<div class="form-group">
						<?= Html::submitButton('Update', [
						'class' => 'update-btn save-changes pull-right',
						'name' => 'updatePassword'
						]) ?>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</section>