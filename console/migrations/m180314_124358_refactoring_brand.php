<?php

use yii\db\Migration,
    common\models\Brands,
    common\models\CategoriesBrands;

/**
 * Class m180314_124358_refactoring_brand
 */
class m180314_124358_refactoring_brand extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach (Brands::find()->all() as $brand) {
            if ($brand->category_id) {
                $model = new CategoriesBrands();
                $model->brand_id = $brand->id;
                $model->category_id = $brand->category_id;
                $model->save();
                if ($brand->category_id == 2) {
                    for ($i = 2; $i < 7; $i++) {
                        $model = new CategoriesBrands();
                        $model->brand_id = $brand->id;
                        $model->category_id = $i;
                        $model->save();
                    }
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180314_124358_refactoring_brand cannot be reverted.\n";

        return false;
    }

}
