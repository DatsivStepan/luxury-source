function initMap() {
    var uluru = {lat: 49.351552, lng: 23.508559};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: {
            url:'/images/map-marker.png', 
            scaledSize: new google.maps.Size(60, 55), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        }
    });
}


$(document).ready(function(){
    var paginations = {
        limit: 16,
        offset: 0
    };
    
    if($('#map').length){
        initMap();        
    }
    var placeholder_comment = '';
    if($('.placeholder_comment').length){
        placeholder_comment = $('.placeholder_comment').val();
    }
    var placeholder_src = '';
    if($('.placeholder_src').length){
        placeholder_src = $('.placeholder_src').val();
    }
    
    function number_format(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                        .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
                .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
                .join('0');
        }
        return s.join(dec);
    }
    
    if($('.productsBlock').length){
        getProducts();        
    }
    
    $(document).on('click', '.show_more_product_btn', function(){
        paginations.offset += paginations.limit;
        getProducts();
    })
    
    
    
    function getProducts(){
        var category_id = $('[name=filter_category_id]').val() 

        $.ajax({
                type: 'POST',
                url: '../../../category/getproducts',
                data: { category_id:category_id,
                limit:paginations.limit, offset:paginations.offset},
                dataType:'json',
                success: function(response){
                    //$('.productsBlock').html('');
                    if(response.length == 0){
                        if(paginations.offset == 0){
                            $('.productsBlock').append('<span style=font-size:16px;>Объявлений не найдено, приходите еще, возможно скоро кто-нибудь добавить то, что Вам нужно.</span>');
                        }
                    }
                    if(response.length < 16){
                        $(".show_more_product_btn").hide();
                    }
                    for(var key in response){
                        var product = response[key];
                        var product_img = '/images/product_add_photo.png';
                        if((product['img_src']!= null) && (product['img_src']!= '')){
                            product_img = product['img_src'];
                        }
                        var price = number_format(product['price'],0,' ',' ');
                            $('.productsBlock').append('<div class="col-xs-12 col-sm-6 col-lg-4">'+
                                        '<div class="thumbnail thumbnail-mod-1 product product_'+product['id']+'_class" style="padding-top:0px;">'+
                                            '<div class="productBlock" style="background-image:url(/'+product_img+');">'+
                                            '</div>'+
                                            '<div class="caption-mod-1">'+
                                                '<h6>'+
                                                    '<a href="/product/'+product['id']+'">'+product['name']+'</a>'+
                                                '</h6>'+
                                              '<div class="caption-footer text-left">'+
                                                '<span class="h4">'+price+'грн</span>'+
                                                '<span class="text-gray">20$</span>'+
                                                '<span data-product_id="'+product['id']+'" class="icon icon-primary icon-shop fa-shopping-cart addProducToCart pull-right"></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>');                        
                    }
                }
        });
    }
    
    
    
	$(document).on('click','.category-list-menu>li.eHover>h6>a', function(e){
            e.preventDefault();
            var menu_li = $(this).parent().parent();
            if(menu_li.hasClass( "active" )){
                menu_li.find('.submenuFirst').hide();
                menu_li.removeClass('active');
            }else{
                menu_li.find('.submenuFirst').show()
                menu_li.addClass('active');            
            }
        });
        
        
        
        
        function getCategoryElement(thisElement){
                    var category_id = thisElement.data('category_id');
                    thisElement.find('.block-sub-menu').show();
                    if($('.block-sub-menu').html() == ''){
                        thisElement.find('.block-sub-menu').html('<div style="text-align:center;color:white;">Pleace wait...</div>');
                        if(category_id != ''){
                            $.ajax({
                                type: 'POST',
                                url: '../../../site/getchildcategory',
                                data: {category_id:category_id},
                                dataType:'json',
                                success: function(response){
                                    thisElement.find('.block-sub-menu').html('');
                                    thisElement.find('.block-sub-menu').append('<ul class="subMenu"></ul>');
                                    for(var categ_id in response){
                                        var category = response[categ_id];
                                        console.log(category);
                                        thisElement.find('.block-sub-menu').find('.subMenu').append('<li><p>'+
                                                '<a href="/category/'+categ_id+'">'+category.name+'</a></p>'+
                                                '<ul class="subMenuLi'+categ_id+'"></ul></li>');


                                        if(category.child.length  != 0){
                                            for(var categ_c_id in category.child){
                                                thisElement.find('.subMenuLi'+categ_id).append('<li>'+
                                                        '<a href="'+categ_c_id+'">'+category.child[categ_c_id]+'</a>'+
                                                        '</li>');                                            
                                            }
                                        }


                                    }
                                }
                            });
                        }else{
                            $('.block-sub-menu').hide();                        
                        }
                        
                    }
        }
        
        
        $(".category-list-menu li.eHover").hover(function() {
                var thisElement = $(this);
                thisElement.find('h6>a').css('background-color', '#e21f2f')
                getCategoryElement(thisElement);
            },function(){
                var classH = $(this).find('.block-sub-menu');
                var thisElement = $(this);
                if(classH.length){
                    setTimeout(function () {
                        if (classH.is(':hover')) {
                        } else {
                            classH.hide();
                            thisElement.find('h6>a').css('background-color', 'transparent')
                        }
                    }, 100);
                }
            }
        );
        
    
    
    
    
    
    
    
    
    
    
    
    
//    $.cookie("product_list", null, { path: ''});
//    $.cookie("product_list", null, { path: '/cart'});
//    $.cookie("product_list", null, { path: '/category/'});
//    $.cookie("product_list", null, { path: '/product/'});
//    $.cookie("order_list", null, { path: ''});
//    $.cookie("order_list", null, { path: '/cart'});
//    $.cookie("order_list", null, { path: '/category/'});
//    $.cookie("order_list", null, { path: '/product/'});
    function changeHeaderCartColor(){
        $('.headerCart').css('background-color', 'transparent')
    }
    
        function reboteCart(){            
            $('.headerCart').css('background-color', '#e21f2f')
            setTimeout(changeHeaderCartColor, 1000);
            $('.headerCart').find('span').text('0')
            if($.cookie('order_list')){
                if($.cookie('order_list') != 'null'){
                    $('.headerCart').find('span').text(parseInt($('.headerCart').find('span').text()) + parseInt(jQuery.parseJSON($.cookie('order_list')).length));
                }
            }
            if($.cookie('product_list')){
                if($.cookie('product_list') != 'null'){
                    $('.headerCart').find('span').text(parseInt($('.headerCart').find('span').text()) + parseInt(jQuery.parseJSON($.cookie('product_list')).length));
                }
            }            
        }
    
        if($.cookie('order_list')){
            if($.cookie('order_list') != 'null'){
                $('.headerCart').find('span').text(parseInt($('.headerCart').find('span').text()) + parseInt(jQuery.parseJSON($.cookie('order_list')).length));
            }
        }
        if($.cookie('product_list')){
            if($.cookie('product_list') != 'null'){
                $('.headerCart').find('span').text(parseInt($('.headerCart').find('span').text()) + parseInt(jQuery.parseJSON($.cookie('product_list')).length));
            }
        }
        
        
        if($('.sectionCartProduct').length){
            if($.cookie('product_list')){
                if(($.cookie('product_list') != 'null') && ($.cookie('product_list') != '[]')){
                    var product_list = jQuery.parseJSON($.cookie('product_list'));
                    $('.sectionCartProduct').show()
                    $.ajax({
                        type: 'POST',
                        url: '../../../site/getproductscart',
                        data: {product_list:product_list},
                        dataType:'json',
                        success: function(response){
                            for(var key in response){
                                var product = response[key];
                                var sum_price = parseInt(product.model['price']) * parseInt(product.count);
                                $('.sectionCartProduct').find('table tbody').append('<tr class="cartProduct'+product.model['id']+'">'+
                                  '<td>'+
                                    '<a href="/product/'+product.model['id']+'">'+product.model['name']+'</a>'+
                                  '</td>'+
                                  '<td>'+product.model['price']+'грн</td>'+
                                  '<td>'+
                                    '<div class="cartQuantity'+product.model['id']+' quantity">'+
                                      '<label>'+
                                        '<span class="icon fa-angle-left icon-gray quantity_cart" data-action="minus" data-product_id="'+product.model['id']+'"></span>'+
                                            '<input value="'+product.count+'" name="Product['+product.model['id']+']" class="product_count" readonly>'+
                                        '<span class="icon fa-angle-right icon-gray  quantity_cart" data-action="plus"  data-product_id="'+product.model['id']+'"></span>'+
                                      '</label>'+
                                    '</div>'+
                                  '</td>'+
                                  '<td class="sum_price">'+sum_price+'грн</td>'+
                                  '<td><span class="icon icon-xs fa-trash icon-gray cartProductDelete" data-product_id="'+product.model['id']+'"></span></td>'+
                                '</tr>');                                
                            }
                        }
                    })
                }
            }
        }
        $(document).on('click', '.quantity_cart', function(){
            var action = $(this).data('action');
            var product_count = $(this).parent().find('.product_count').val()
            var product_id = $(this).data('product_id');
            if(action == 'minus'){
                if(product_count > '1'){
                    product_count = parseInt(product_count) - 1;                    
                }
            }else{
                product_count = parseInt(product_count) + 1;                
            }
            $(this).parent().find('.product_count').val(product_count)
            var product_list = jQuery.parseJSON($.cookie('product_list'));
            product_list = jQuery.grep(product_list, function(value) {
                return value != product_id;
            });
            for (var i = 1; i <= parseInt(product_count); i++) {
                product_list.push(product_id);
            }
            $.cookie('product_list', JSON.stringify(product_list))
            $.cookie("product_list", JSON.stringify(product_list), { path: '/'});
            $.cookie("product_list", JSON.stringify(product_list), { path: '/cart'});
            $.cookie("product_list", JSON.stringify(product_list), { path: '/category/'});
            $.cookie("product_list", JSON.stringify(product_list), { path: '/product/'});
            reboteCart();
        });
        
        
        
        $(document).on('click', '.cartOrderDelete', function(){
            var order_id = $(this).data('order_id')
            $('.cartOrder'+order_id).remove();
            var order_list = jQuery.parseJSON($.cookie('order_list'));
            order_list = jQuery.grep(order_list, function(value) {
                return value.id != order_id;
            });
            $.cookie('order_list', JSON.stringify(order_list))
            $.cookie("order_list", JSON.stringify(order_list), { path: '/'});
            $.cookie("order_list", JSON.stringify(order_list), { path: '/cart'});
            $.cookie("order_list", JSON.stringify(order_list), { path: '/category/'});
            $.cookie("order_list", JSON.stringify(order_list), { path: '/product/'});
            reboteCart();
        });
        
        $(document).on('click', '.cartProductDelete', function(){
            var product_id = $(this).data('product_id');
            $('.cartProduct'+product_id).remove();
            var product_list = jQuery.parseJSON($.cookie('product_list'));
            product_list = jQuery.grep(product_list, function(value) {
                return value != product_id;
            });
            if(JSON.stringify(product_list) == '[]'){
                $('.sectionCartProduct').hide()
            }
            $.cookie('product_list', JSON.stringify(product_list))
            $.cookie("product_list", JSON.stringify(product_list), { path: '/'});
            $.cookie("product_list", JSON.stringify(product_list), { path: '/cart'});
            $.cookie("product_list", JSON.stringify(product_list), { path: '/category/'});
            $.cookie("product_list", JSON.stringify(product_list), { path: '/product/'});
            reboteCart();
        });
        
        $(document).on('change','.cart_order_src, .cart_order_comment', function(){
            var order_id = $(this).data('order_id');
            var order_src = $('.cartOrder'+order_id).find('.cart_order_src').val()
            var order_comment = $('.cartOrder'+order_id).find('.cart_order_comment').val()
            var order_list = [];
            if($.cookie('order_list')){
                if($.cookie('order_list') != 'null'){
                    order_list = jQuery.parseJSON($.cookie('order_list'));
                    order_list = jQuery.grep(order_list, function(value) {
                        return value.id != order_id;
                    });
                }
            }
           
            order_list.push({id:order_id,src:order_src,comment:order_comment});
            $.cookie('order_list', JSON.stringify(order_list))
            $.cookie("order_list", JSON.stringify(order_list), { path: '/'});
            $.cookie("order_list", JSON.stringify(order_list), { path: '/cart'});
            $.cookie("order_list", JSON.stringify(order_list), { path: '/category/'});
            $.cookie("order_list", JSON.stringify(order_list), { path: '/product/'});
        });
        
        if($('.sectionCartOrder').length){
                if($.cookie('order_list')){
                    if($.cookie('order_list') != 'null'){
                        $('.sectionCartOrder').show()
                        var order_list = jQuery.parseJSON($.cookie('order_list'));
                        
                        for(var key in order_list){
                            var order = order_list[key];
                            $('.sectionCartOrder table tbody').append('<tr class="cartOrder'+order['id']+'">'+
                                '<td><span class="icon icon-xs fa-trash icon-gray cartOrderDelete" data-order_id="'+order['id']+'"></span></td>'+
                                '<td><input type="url"  name="Order['+order['id']+'][src]"  class="cart_order_src formStyle"  data-order_id="'+order['id']+'"  value="'+order['src']+'" placeholder="'+placeholder_src+'"></td>'+
                                '<td><textarea  name="Order['+order['id']+'][comment]" class="cart_order_comment formStyle"  data-order_id="'+order['id']+'" placeholder="'+placeholder_comment+'" rows="1" style="max-height: 110px;max-width:500px;margin: 0px">'+order['comment']+'</textarea></td>'+
                              '</tr>')
                        }                    
                    }else{
                        addNewCarOrderInputs();
                    }
                }else{
                    addNewCarOrderInputs();                    
                }
        }
        function addNewCarOrderInputs(){
            var now_t = $.now();
            $('.sectionCartOrder table tbody').append('<tr class="cartOrder'+now_t+'">'+
                '<td><span class="icon icon-xs fa-trash icon-gray cartOrderDelete" data-order_id="'+now_t+'"></span></td>'+
                '<td><input type="url"  name="Order['+now_t+'][src]"  class="cart_order_src formStyle"  data-order_id="'+now_t+'"  placeholder="'+placeholder_src+'" ></td>'+
                '<td><textarea name="Order['+now_t+'][comment]" class="cart_order_comment formStyle"  data-order_id="'+now_t+'" placeholder="'+placeholder_comment+'" rows="1" style="max-height: 110px;max-width:500px;margin: 0px"></textarea></td>'+
              '</tr>');
        }
        
        $(document).on('click', '.cartOrderPlus', function(){
            addNewCarOrderInputs();
        })


        function addProductToList(product_id){
            if($.cookie('product_list')){
                if($.cookie('product_list') != 'null'){
                    var product_list = jQuery.parseJSON($.cookie('product_list'));
                    product_list.push(product_id);
                    $.cookie('product_list', JSON.stringify(product_list))
                    $.cookie("product_list", JSON.stringify(product_list), { path: '/'});
                    $.cookie("product_list", JSON.stringify(product_list), { path: '/cart'});
                    $.cookie("product_list", JSON.stringify(product_list), { path: '/category/'});
                    $.cookie("product_list", JSON.stringify(product_list), { path: '/product/'});
                }else{
                    var product_list = [];
                    product_list.push(product_id);
                    $.cookie('product_list', JSON.stringify(product_list))
                    $.cookie("product_list", JSON.stringify(product_list), { path: '/'});
                    $.cookie("product_list", JSON.stringify(product_list), { path: '/cart'});
                    $.cookie("product_list", JSON.stringify(product_list), { path: '/category/'});
                    $.cookie("product_list", JSON.stringify(product_list), { path: '/product/'});
                }
            }else{
                var product_list = [];
                product_list.push(product_id);
                $.cookie('product_list', JSON.stringify(product_list))
                $.cookie("product_list", JSON.stringify(product_list), { path: '/'});
                $.cookie("product_list", JSON.stringify(product_list), { path: '/cart'});
                $.cookie("product_list", JSON.stringify(product_list), { path: '/category/'});
                $.cookie("product_list", JSON.stringify(product_list), { path: '/product/'});
            }
            reboteCart();
        }

        function addOrderToList(order_src){
            if($.cookie('order_list')){
                if($.cookie('order_list') != 'null'){
                    var order_list = jQuery.parseJSON($.cookie('order_list'));
                    order_list.push({id:$.now(),src:order_src,comment:''});
                    $.cookie('order_list', JSON.stringify(order_list))
                    $.cookie("order_list", JSON.stringify(order_list), { path: '/'});
                    $.cookie("order_list", JSON.stringify(order_list), { path: '/cart'});
                    $.cookie("order_list", JSON.stringify(order_list), { path: '/category/'});
                    $.cookie("order_list", JSON.stringify(order_list), { path: '/product/'});
                }else{
                    var order_list = [];
                    order_list.push({id:$.now(),src:order_src,comment:''});                        
                    $.cookie('order_list', JSON.stringify(order_list))
                    $.cookie("order_list", JSON.stringify(order_list), { path: '/'});
                    $.cookie("order_list", JSON.stringify(order_list), { path: '/cart'});
                    $.cookie("order_list", JSON.stringify(order_list), { path: '/category/'});
                    $.cookie("order_list", JSON.stringify(order_list), { path: '/product/'});
                }
            }else{
                var order_list = [];
                order_list.push({id:$.now(),src:order_src,comment:''});
                $.cookie('order_list', JSON.stringify(order_list))
                $.cookie("order_list", JSON.stringify(order_list), { path: '/'});
                $.cookie("order_list", JSON.stringify(order_list), { path: '/cart'});
                $.cookie("order_list", JSON.stringify(order_list), { path: '/category/'});
                $.cookie("order_list", JSON.stringify(order_list), { path: '/product/'});
            }
            yaCounter45580380.reachGoal('SEND_URL');
            window.location.replace("/cart");
        }
    
    
    
        
        $('.orderForm').submit(function( event ) {
            event.preventDefault();
            var order_src = $(this).find('[name=order_src]').val();
            if(order_src.length < 10){
            }else{
                addOrderToList(order_src);
            }
        });

        $(document).on('click', '.addProducToCart', function(){
            var product_id = $(this).data('product_id')
//             $(".product_"+product_id+"_class")  
//              .clone()  
//              .css({'position' : 'absolute', 'z-index' : '100'})  
//              .appendTo(".product_"+product_id+"_class")  
//              .animate({opacity: 0.5,  
//                            position: 'absolute',
//                            top: '-100px',
//                            //marginLeft: 1700, /* Важно помнить, что названия СSS-свойств пишущихся  
//                            //через дефис заменяются на аналогичные в стиле "camelCase" */  
//                            width: 50,   
//                            height: 50}, 12700, function() {  
//                    $(this).remove();  
//              });  
            addProductToList(product_id);
        })
    
    
    
        $('.cartButtonForm').click(function( event ) {

            if((($.cookie('order_list') != 'null') && ($.cookie('order_list') != '[]')) || (($.cookie('product_list') != 'null') && ($.cookie('product_list') != '[]'))){
                    $.cookie('order_list', null)
                    $.cookie("order_list", null, { path: '/'});
                    $.cookie("order_list", null, { path: '/cart'});
                    $.cookie("order_list", null, { path: '/category/'});
                    $.cookie("order_list", null, { path: '/product/'});
                    
                    $.cookie('product_list', null)
                    $.cookie("product_list", null, { path: '/'});
                    $.cookie("product_list", null, { path: '/cart'});
                    $.cookie("product_list", null, { path: '/category/'});
                    $.cookie("product_list", null, { path: '/product/'});
                    yaCounter45580380.reachGoal('ORDER');
                    
                }else{
                    event.preventDefault()
                        swal({
                            title: "Корзина пуста!",
                            text: "Можливо ви хочете замовити дзвінок, і ми підберемо для вас потрібний товар",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Так, замовити",
                            cancelButtonText: "Ні, не потрібно",
                            closeOnConfirm: false
                          },
                          function(){
                            //swal("Deleted!", "", "success");
                          });
                }
        });
    
    
    
    
    
});

function openAskQuestionPage(){
	document.querySelector('.ask-question').style.display = 'block';

}
function closeAskQuestion() {
	document.querySelector('.ask-question').style.display = 'none';
}
function openComments(){
	var element = document.querySelector('.hide_comments');
	if(element.style.display != 'none'){
		element.style.display = 'none';
	} else {
		element.style.display = 'block';
	}
}

//Counter
var Counter = (function(){
	'use strict'
	var TEMPLATE = '<span class="number-item">Количество</span><input type="text" placeholder="0">' +
				   '<button class="plus_number_goods">+</button>' +
				   '<button class="minus_number_goods">-</button>' +
				   '<span class="number_of_pieces">шт.</span>';

	function Counter(element){
		var _this = this;
		if(!element && !(element instanceof HTMLElement)) return;
		element.innerHTML = TEMPLATE;
		this.input = element.querySelector('input');
		this.input.parentElement.querySelector('.plus_number_goods').addEventListener('click', function(){ _this.plus() });
		this.input.parentElement.querySelector('.minus_number_goods').addEventListener('click', function(){ _this.minus() });
		this.input.value = 0;
	}

	Counter.prototype.plus = function(){
		this.input.value = parseInt(this.input.value) + 1;
	}

	Counter.prototype.minus = function(){
		this.input.value = parseInt(this.input.value) > 0 ? parseInt(this.input.value) - 1 : 0;
	}

	Counter.init = function(){
		var counters = document.querySelectorAll('[data-hr-counter]');
		for(var i = 0; i < counters.length; i++){
			Counter.counters.push(new Counter(counters[i]));
		}
	}

	Counter.counters  = [];

	return Counter;
})();

window.addEventListener('load', function(){
	Counter.init();
});


