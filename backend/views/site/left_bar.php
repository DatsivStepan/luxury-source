<?php

use yii\helpers\Html;
use common\models\Configuration;
use common\components\AdminMenuUrlManager;
$modelMenuUrl = new AdminMenuUrlManager();
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/admin/images/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= \Yii::$app->user->identity->username; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ADMIN NAVIGATION</li>
        <li>
          <a href="/admin">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Pages</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('pages/pages/create'); ?>"><a href="/admin/pages/pages/create"><i class="fa fa-circle-o"></i> Add page</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('pages/pages/index'); ?>"><a href="/admin/pages/pages/index"><i class="fa fa-circle-o"></i> List pages</a></li>
          </ul>
        </li>
        <li class="treeview admin-hide">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Users</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('users/users/create'); ?>"><a href="/admin/users/users/create"><i class="fa fa-circle-o"></i> Add user</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('users/users/index'); ?>"><a href="/admin/users/users/index"><i class="fa fa-circle-o"></i> List users</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-bag"></i>
            <span>Products</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('products/products/create'); ?>"><a href="/admin/products/products/create"><i class="fa fa-circle-o"></i> Add product</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('products/products/index'); ?>"><a href="/admin/products/products/index"><i class="fa fa-circle-o"></i> List products</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-bold"></i>
            <span>Brands</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('brands/brands/create'); ?>"><a href="/admin/brands/brands/create"><i class="fa fa-circle-o"></i> Add brand</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('brands/brands/index'); ?>"><a href="/admin/brands/brands/index"><i class="fa fa-circle-o"></i> List brands</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-bold"></i>
            <span>Models</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('modells/modells/create'); ?>"><a href="/admin/modells/modells/create"><i class="fa fa-circle-o"></i> Add model</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('modells/modells/index'); ?>"><a href="/admin/modells/modells/index"><i class="fa fa-circle-o"></i> List models</a></li>
          </ul>
        </li>
        <li class="treeview admin-hide">
          <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span>Shops</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('shops/shops/create'); ?>"><a href="/admin/shops/shops/create"><i class="fa fa-circle-o"></i> Add shop </a></li>
            <li class="<?= $modelMenuUrl->checkUrl('shops/shops/index'); ?>"><a href="/admin/shops/shops/index"><i class="fa fa-circle-o"></i> List shops </a></li>
          </ul>
        </li>
        <li class="treeview admin-hide">
          <a href="#">
            <i class="fa fa-tasks"></i>
            <span>Categories</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('categories/categories/create'); ?>"><a href="/admin/categories/categories/create"><i class="fa fa-circle-o"></i> Add category</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('categories/categories/index'); ?>"><a href="/admin/categories/categories/index"><i class="fa fa-circle-o"></i> List categories</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('products/attributes/create'); ?>"><a href="/admin/products/attributes/create"><i class="fa fa-circle-o"></i> Add attribute</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('products/attributes/index'); ?>"><a href="/admin/products/attributes/index"><i class="fa fa-circle-o"></i> List attributes</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs"></i>
            <span>Settings</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('configuration/configuration/index'); ?>">
                <a href="/admin/configuration/configuration/index"><i class="fa fa-circle-o"></i> Configuration</a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('language/source-message/index'); ?>">
                <a href="/admin/language/source-message/index"><i class="fa fa-circle-o"></i> Translation</a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('language/language/index'); ?>">
                <a href="/admin/language/language/index"><i class="fa fa-circle-o"></i> Languages</a>
            </li>
          </ul>
        </li>
        <li class="treeview admin-hide">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>Posts</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('posts/posts/create'); ?>"><a href="/admin/posts/posts/create"><i class="fa fa-circle-o"></i> Add post</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('posts/posts/index'); ?>"><a href="/admin/posts/posts/index"><i class="fa fa-circle-o"></i> List posts</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>