<?php

use yii\helpers\Html;

?>
<?php /* ?>
<style>
    .admin-hide{
        display:none;
    }
</style>
<?php */ ?>
<header class="main-header">
    <!-- Logo -->
    <a href="/admin" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="/admin/images/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?= \Yii::$app->user->identity->username; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="/admin/images/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <?= \Yii::$app->user->identity->username; ?>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                    <?= Html::beginForm(['/site/logout'], 'post'); ?>
                        <?= Html::submitButton(
                            'Sign out',
                            ['class' => 'btn btn-default btn-flat']
                        ); ?>
                    <?= Html::endForm() ?>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>