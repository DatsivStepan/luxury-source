<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\users\models\SearchUsers */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        <div class="user-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'username',
                    'email:email',
                    // 'status',
                    // 'created_at',
                    // 'updated_at',
                    // 'description:ntext',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'delete' => function ($url, $model) {
                                return Html::button(
                                    '<i class="glyphicon glyphicon-trash"></i>',
                                    ['delete', 'id' => $model->id],
                                    [
                                        'title' => 'delete', 
                                        'data-pjax' => 0, 
                                        'aria-label' => "Delete", 
                                        'data-method' => "post", 
                                        'data-confirm' => "Are you sure you want to delete this item?", 
                                        'class' => 'btn btn-sm btn-danger'
                                    ]
                                );
                            },
                            'update' => function ($url, $model) {
                                return Html::a(
                                    '<i class="glyphicon glyphicon-pencil"></i>',
                                    ['update', 'id' => $model->id],
                                    ['data-pjax' => 0, 'class' => 'btn btn-sm btn-primary']
                                );
                            }
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>
