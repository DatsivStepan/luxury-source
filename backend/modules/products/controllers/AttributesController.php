<?php

namespace backend\modules\products\controllers;

use Yii;
use common\models\Attributes;
use backend\modules\products\models\SearchAttributes;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Categories;
use common\models\AttributesValue;
use common\models\CategoriesAttributes;
use yii\helpers\ArrayHelper;

/**
 * AttributesController implements the CRUD actions for Attributes model.
 */
class AttributesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * Lists all Attributes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchAttributes();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Attributes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Attributes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Attributes();
        $modelNewAttributesValue = new AttributesValue;
        $modelAttributesValue = [null];
        $modelCategories = Categories::find()->all();
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if (Yii::$app->request->post('AttributesValue')) {
                    AttributesValue::saveAttributesOptions(Yii::$app->request->post('AttributesValue'), $model->id);
                }
                if (Yii::$app->request->post('Category')) {
                    CategoriesAttributes::saveAttributesCategory(Yii::$app->request->post('Category'), $model->id);
                }
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelCategories' => $modelCategories,
            'modelAttributesValue' => $modelAttributesValue,
            'modelNewAttributesValue' => $modelNewAttributesValue,
        ]);
    }

    /**
     * Updates an existing Attributes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelNewAttributesValue = new AttributesValue;
        $modelAttributesValue = AttributesValue::findAll(['attribute_id' => $model->id]);
        $modelCategories = Categories::find()->all();
        $modelAttributesCategory = 
                ArrayHelper::map(CategoriesAttributes::findAll(['attribute_id' => $model->id]), 'category_id', 'attribute_id');
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if (Yii::$app->request->post('AttributesValue')) {
                    AttributesValue::saveAttributesOptions(Yii::$app->request->post('AttributesValue'), $model->id);
                }
                if (Yii::$app->request->post('Category')) {
                    CategoriesAttributes::saveAttributesCategory(Yii::$app->request->post('Category'), $model->id);
                }
                return $this->redirect(['index']);
            }
        }
            
        return $this->render('update', [
            'model' => $model,
            'modelCategories' => $modelCategories,
            'modelAttributesCategory' => $modelAttributesCategory,
            'modelAttributesValue' => $modelAttributesValue ? $modelAttributesValue : [$modelAttributesValue],
            'modelNewAttributesValue' => $modelNewAttributesValue,
        ]);
    }

    /**
     * Deletes an existing Attributes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Attributes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attributes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Attributes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
