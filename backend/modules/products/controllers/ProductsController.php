<?php

namespace backend\modules\products\controllers;

use Yii;
use common\models\Products;
use backend\modules\products\models\SearchProducts;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use common\models\Brands;
use common\models\CategoriesAttributes;
use common\models\ProductsAttributes;
use yii\web\Response;
use common\models\Shops;
use common\models\ProductsImages;
use common\models\AttributesValue;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchProducts();
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionGetCategoryAttribute($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $res = ['status' => false, 'data' => null];
        
        $modelCA = CategoriesAttributes::findAll(['category_id' => $id]);
        if ($modelCA) {
            $res['status'] = true;
            foreach($modelCA as $model){
                if ($model->attributesModel) {
                    $attribute = $model->attributesModel;
                    $res['data'][$attribute->id] = [
                        'name' => $attribute->name,
                        'type' => $attribute->type,
                        'attributeValue' => AttributesValue::find()
                            ->where(['attribute_id' => $attribute->id])->asArray()->all(),
                    ];
                }
                
            }
        }

        return $res;
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();
        $arrayCategories = ArrayHelper::map(Categories::find()->all(), 'id', 'name');
        $arrayShops = ArrayHelper::map(Shops::find()->all(), 'id', 'name');
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if (Yii::$app->request->post('ProductsAttributes')) {
                    ProductsAttributes::saveProductAttributes(Yii::$app->request->post('ProductsAttributes'), $model->id);
                }
                if (Yii::$app->request->post('Products')['images']) {
                    ProductsImages::saveProductImages(
                        Yii::$app->request->post('Products')['images'],
                        Yii::$app->request->post('Products')['imagesSort'],
                        $model->id
                    );
                }
            }

            return $this->redirect(['index']);
        } 

        return $this->render('create', [
            'model' => $model,
            'arrayShops' => $arrayShops,
            'arrayCategories' => $arrayCategories,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $arrayCategories = ArrayHelper::map(Categories::find()->all(), 'id', 'name');
        $arrayShops = ArrayHelper::map(Shops::find()->all(), 'id', 'name');
        $arrayBrands = $model->category_id ? Brands::getAllArrayMapByCategory() : null;

        $modelCategoryAttribute = $model->category_id ? 
                CategoriesAttributes::find()
                        ->where(['category_id' => $model->category_id])
                        ->all() : null;
        $modelProductsAttributes = $model->category_id ? 
                ArrayHelper::map(ProductsAttributes::find()
                        ->where(['product_id' => $model->id])
                        ->all(), 'attribute_id', 'value') : null;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if (Yii::$app->request->post('ProductsAttributes')) {
                    ProductsAttributes::saveProductAttributes(Yii::$app->request->post('ProductsAttributes'), $model->id);
                }
                if (Yii::$app->request->post('Products')['images']) {
                    ProductsImages::saveProductImages(
                        Yii::$app->request->post('Products')['images'],
                        Yii::$app->request->post('Products')['imagesSort'],
                        $model->id
                    );
                }
            }
            return $this->redirect(['update', 'id' => $model->id]);
        } 
        
        return $this->render('update', [
            'model' => $model,
            'arrayCategories' => $arrayCategories,
            'arrayBrands' => $arrayBrands,
            'arrayShops' => $arrayShops,
            'modelCategoryAttribute' => $modelCategoryAttribute,
            'modelProductsAttributes' => $modelProductsAttributes,
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    
    public function actionSavePhoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/products/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.png';  //5
            move_uploaded_file($tempFile,$targetFile); //6
            return 'admin/images/products/'.\Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.png'; //5
        }
    }
    
    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
