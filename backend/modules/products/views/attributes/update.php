<?php

use yii\helpers\Html,
    yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\Attributes */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Attributes',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Attributes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        
        <div class="attributes-update">

            <?= $this->render('_form', [
                'model' => $model,
                'modelCategories' => $modelCategories,
                'modelAttributesCategory' => $modelAttributesCategory,
                'modelAttributesValue' => $modelAttributesValue,
                'modelNewAttributesValue' => $modelNewAttributesValue,
            ]) ?>

        </div>

    </section>