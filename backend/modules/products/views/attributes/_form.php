<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Attributes;
use kartik\checkbox\CheckboxX;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Attributes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attributes-form">

    <?php $form = ActiveForm::begin(['id' => 'attributes-form']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(Attributes::$typeArray) ?>

    <div class="well js-attribute-options <?= (!$model->type || ($model->type == Attributes::TYPE_TEXT)) ? 'hide' : '' ?>">
        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'item_block',
            'widgetBody' => '.container-item',
            'widgetItem' => '.item',
            'limit' => 20,
            'min' => -1,
            'insertButton' => '.add-item',
            'deleteButton' => '.remove-item',
            'model' => $model,
            'formId' => 'attributes-form',
            'formFields' => [
                'value',
                'sort',
            ],
        ]) ?>

            <div class="container-item">
                <?php foreach ($modelAttributesValue as $item) { ?>
                    <div class="item panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-right">
                                <?= Html::button(
                                    Html::tag('i', null, ['class' => 'glyphicon glyphicon-minus']),
                                    ['class' => 'remove-item btn btn-sm btn-danger btn-sm']
                                ) ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="panel-body">
                            <div class="row form-group" style="margin-bottom: 0px">
                                <div class="col-sm-9 col-xs-9">
                                    <?= $form->field(
                                        $modelNewAttributesValue,
                                        'value[]',
                                        ['inputOptions' => [ 'class' => 'form-control', 'value' => $item ? $item->value : null]]
                                    )->textInput() ?>
                                </div>

                                <div class="col-sm-3 col-xs-3">
                                    <?= $form->field(
                                        $modelNewAttributesValue,
                                        'sort[]',
                                        ['inputOptions' => [ 'class' => 'form-control', 'value' => $item ? $item->sort : null]]
                                    )->input('number') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
            <div>
                <?= Html::button(
                    Html::tag('i', null, ['class' => 'glyphicon glyphicon-plus']),
                    ['class' => 'add-item btn btn-sm btn-default btn-sm pull-right']
                ) ?>
            </div>
            <div class="clearfix"></div>
        <?php DynamicFormWidget::end(); ?>
    </div>
    
    <div class="well">
        <?php foreach ($modelCategories as $category) { ?>
            <?php $value = 0; ?>
            <?php if ($modelAttributesCategory) { ?>
                <?php $value = array_key_exists($category->id, $modelAttributesCategory) ? 1 : 0; ?>
            <?php } ?>
            <div class="col-xs-4">
                <?= CheckboxX::widget([
                    'name' => 'Category[' . $category->id . ']',
                    'value' => $value,
                    'options' => [
                        'class' => ''
                    ],
                    'pluginOptions' => [
                        'threeState' => false
                    ]
                ]); ?>
                <?= $category->name; ?>
            </div>
        <?php } ?>
        <div class="clearfix"></div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(function(){
        $(document).on('change', '#attributes-type', function() {
            console.log($(this).val())
            if ($(this).val() == <?= Attributes::TYPE_TEXT; ?>) {
                $('.js-attribute-options').addClass('hide');
                $('.js-attribute-options .container-item .item').remove();
            } else {
                $('.js-attribute-options').removeClass('hide');
            }
        })
    })
</script>