<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use common\models\Attributes;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="prev">
            <div id="temp" class="file-row">

            </div>
        </div>
    </div>
</div>
<div class="products-form">

    <?php $form = ActiveForm::begin(['id' => 'product-form']); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <div style="height:150px;margin-bottom:10px;">
            <div style="height:120px;">
                <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
                <img src="<?= $model->getImages(); ?>" class="previewProductImage img-thumbnail" id="js-add-product-image" style="height:100%;width:auto;"><br>
            </div>
            <div style="height:30px;">
                <a class="btn btn-sm btn-success addProductImage">Change image</a><br><br>
            </div>
        </div>
    
    <div class="clearfix"></div>
        <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
            'data' => $arrayCategories,
            'language' => 'en',
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Select ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($model, 'brand_id', ['options' => ['class' => 'col-xs-12', 'style' => 'padding: 0']])
            ->widget(DepDrop::className(), [
                'type' => DepDrop::TYPE_SELECT2,
                'data' => $arrayBrands,
                'options' => ['placeholder' => Yii::t('app', 'Select...')],
                'select2Options' => [
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ],
                'pluginOptions' => [
                    'placeholder' => Yii::t('app', 'Select...'),
                    'depends' => ['products-category_id'],
                    'url' => Url::to(['/brands/brands/get-brand-by-category']),
                ],
            ]) ?>
        <?= $form->field($model, 'modells_id', ['options' => ['class' => 'col-xs-12', 'style' => 'padding: 0']])
            ->widget(DepDrop::className(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => ['placeholder' => Yii::t('app', 'Select...')],
                'select2Options' => [
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ],
                'pluginOptions' => [
                    'placeholder' => Yii::t('app', 'Select...'),
                    'initialize' => true,
                    'initDepends' => ['products-brand_id'],
                    'loadingText' => Yii::t('app', 'Please wait'),
                    'depends' => ['products-brand_id'],
                    'url' => Url::to(['/modells/modells/get-modells-by-brand']) . '/' . $model->modells_id,
                ],
            ]) ?>

        <div class="clearfix"></div>
            <div class="well">
                <h3>Products attributes</h3>
                <div class="col-sm-12">
                    <div class="js-products-options">
                        <?php if ($modelCategoryAttribute) { ?>
                            <?php foreach($modelCategoryAttribute as $categoryAttribute) { ?>
                                <?php $attribute = $categoryAttribute->attributesModel ? $categoryAttribute->attributesModel : null; ?>
                                <?php if(!$attribute) { continue; } ?>
                                <div>
                                    <h4><?= $attribute->name;?></h4>
                                    <?php 
                                        $attributeValueArray = $attribute->attributesValueModel ? $attribute->getAttributesValueArray() : [];
                                        $value = $modelProductsAttributes ? (array_key_exists($attribute->id, $modelProductsAttributes) ? $modelProductsAttributes[$attribute->id] : null ) : null;

                                        switch ($attribute->type) {
                                            case Attributes::TYPE_TEXT:
                                                echo Html::textInput('ProductsAttributes[' . $attribute->id . ']', $value, ['class' => 'form-control']);
                                                break;
                                            case Attributes::TYPE_SELECT:
                                                echo Html::dropDownList('ProductsAttributes[' . $attribute->id . ']', $value, $attributeValueArray, ['class' => 'form-control', 'prompt' => 'Select...']);
                                                break;
                                            case Attributes::TYPE_CHECKBOX:
                                                echo Html::checkboxList('ProductsAttributes[' . $attribute->id . ']', json_decode($value), $attributeValueArray);
                                                break;
                                            case Attributes::TYPE_RADIO:
                                                echo Html::radioList('ProductsAttributes[' . $attribute->id . ']', $value, $attributeValueArray);
                                                break;
                                        }
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        <div class="well">
            <h3>Products Images</h3>
            <div class="col-xs-12">
                    <table class="table table-striped table-bordered table-hover js-table-images">
                        <tbody class="container-item">
                            <?php if ($model->productsImagesModel) { ?>
                                <?php foreach ($model->productsImagesModel as $key => $productsImages) { ?>
                                    <tr class="js-item">
                                        <td style="max-width: 200px;vertical-align: middle;">
                                            <div style="width:150px;height:150px;">
                                                <?= $form->field($model, 'images[]')->hiddeninput(['value' => $productsImages->img_src])->label(false); ?>
                                                <img src="/<?= $productsImages->img_src; ?>" class=" img-thumbnail"><br>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <?= $form->field(
                                                $model,
                                                'imagesSort[]',
                                                [
                                                    'inputOptions' => [
                                                        'class' => 'form-control',
                                                        'value' => $productsImages->sort,
                                                    ]
                                                ]
                                            )->textInput() ?>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <?= Html::button(
                                                Html::tag('i', null, ['class' => 'glyphicon glyphicon-minus']),
                                                ['class' => 'js-remove-item btn btn-sm btn-danger btn-sm']
                                            ) ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>
                                    <?= Html::button(
                                        Html::tag('i', null, ['class' => 'glyphicon glyphicon-plus']),
                                        ['class' => 'addProductImages add-item btn btn-sm btn-default btn-sm pull-right']
                                    ) ?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="form-group">
            <div class="col-sm-6">
                <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-6">
                <?= $form->field($model, 'price_retail')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(function(){
        
        $(document).on('click', '.js-remove-item', function(){
            $(this).closest('tr.js-item').remove()
        })
        
        $(document).on('change', '#products-category_id', function(){
            var category_id = $(this).val();
            $('.js-products-options').html('')
            if (category_id) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/products/products/get-category-attribute?id=' + category_id,
                    dataType:'json',
                    success: function(response){
                        if (response.status) {
                            var attributes = response.data;
                            $.each( attributes, function( attribute_id, attribute ) {
//                                console.log( attribute)
                                switch (attribute['type']) {
                                    case <?= Attributes::TYPE_TEXT; ?>:
                                        $('.js-products-options').append(
                                            $('<div/>').append(
                                                $('<h4/>', {
                                                    text:attribute['name']
                                                }),
                                                $('<input/>', {
                                                    type: 'text',
                                                    class: 'form-control',
                                                    name: 'ProductsAttributes['+attribute_id+']',
                                                }),
                                                $('<div/>', {
                                                    class:'clearfix'
                                                }),
                                            ),
                                            $('<hr/>')
                                        )
                                        break;
                                    case <?= Attributes::TYPE_SELECT; ?>:
                                        $('.js-products-options').append(
                                            $('<div/>').append(
                                                $('<h4/>', {
                                                    text:attribute['name']
                                                }),
                                                $('<select/>', {
                                                    type: 'text',
                                                    class: 'form-control js-products-select-attributes-' + attribute_id,
                                                    name: 'ProductsAttributes['+attribute_id+']',
                                                }),
                                                $('<div/>', {
                                                    class:'clearfix'
                                                }),
                                            ),
                                            $('<hr/>')
                                        )
                                
                                        if(attribute['attributeValue']) {
                                            $('.js-products-select-attributes-' + attribute_id).append(
                                                $('<option/>', {
                                                    value:'',
                                                    text:'Select...'
                                                })
                                            )
                                            $.each( attribute['attributeValue'], function( key, attributeValue ) {
                                                $('.js-products-select-attributes-' + attribute_id).append(
                                                    $('<option/>', {
                                                        value:attributeValue['id'],
                                                        text:attributeValue['value']
                                                    })
                                                )
                                            })
                                        }
                                        break;
                                    case <?= Attributes::TYPE_CHECKBOX; ?>:
                                                
                                        $('.js-products-options').append(
                                            $('<div/>').append(
                                                $('<h4/>', {
                                                    text:attribute['name']
                                                }),
                                                $('<div/>',{
                                                    class:'js-products-checkbox-attributes-' + attribute_id
                                                })
                                            ),
                                            $('<hr/>')
                                        )
                                        $.each( attribute['attributeValue'], function( key, attributeValue ) {
                                            $('.js-products-checkbox-attributes-' + attribute_id).append(
                                                $('<label/>',{
                                                    text:attributeValue['value']
                                                }).prepend(
                                                    $('<input/>', {
                                                        type: 'checkbox',
                                                        name: 'ProductsAttributes['+attribute_id+'][]',
                                                        value: attributeValue['id'],
                                                    }),
                                                )
                                            )
                                        })
                                        break;
                                    case <?= Attributes::TYPE_RADIO; ?>:
                                        $('.js-products-options').append(
                                            $('<div/>').append(
                                                $('<h4/>', {
                                                    text:attribute['name']
                                                }),
                                                $('<div/>',{
                                                    class:'js-products-radio-attributes-' + attribute_id
                                                })
                                            ),
                                            $('<hr/>')
                                        )
                                        $.each( attribute['attributeValue'], function( key, attributeValue ) {
                                            $('.js-products-radio-attributes-' + attribute_id).append(
                                                $('<label/>',{
                                                    text:attributeValue['value']
                                                }).prepend(
                                                    $('<input/>', {
                                                        type: 'radio',
                                                        name: 'ProductsAttributes['+attribute_id+']',
                                                        value: attributeValue['id'],
                                                    }),
                                                )
                                            )
                                        })
                                        break;
                                }
                            });
                        }
                    }
                })
            }
        })
    })
</script>