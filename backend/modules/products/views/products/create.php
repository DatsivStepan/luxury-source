<?php

use yii\helpers\Html,
    yii\widgets\Breadcrumbs;


/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = Yii::t('app', 'Create Products');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        
        <div class="products-create">

            <?= $this->render('_form', [
                'model' => $model,
                'arrayCategories' => $arrayCategories,
                'arrayShops' => $arrayShops,
                'arrayBrands' => null,
                'modelCategoryAttribute' => null,
                'modelProductsAttributes' => null,
                'modelImagesArray' => null
            ]) ?>

        </div>

    </section>
