<?php

use yii\helpers\Html,
    yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Products',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        
        <div class="products-update">

            <?= $this->render('_form', [
                'model' => $model,
                'arrayShops' => $arrayShops,
                'arrayCategories' => $arrayCategories,
                'arrayBrands' => $arrayBrands,
                'modelCategoryAttribute' => $modelCategoryAttribute,
                'modelProductsAttributes' => $modelProductsAttributes,
            ]) ?>

        </div>

    </section>