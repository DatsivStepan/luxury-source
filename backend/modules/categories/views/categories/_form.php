<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use common\models\CategoriesAttributes;
use kartik\select2\Select2;
use common\models\Products;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use common\models\ProductsSponsored;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */

$productArray = !$model->isNewRecord ? ArrayHelper::map(Products::getAllAdmin($model->id), 'id', 'name') : [];
?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>
<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text_slide_1', ['options' => ['class' => 'col-sm-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text_slide_2', ['options' => ['class' => 'col-sm-6']])->textInput(['maxlength' => true]) ?>
    
    <div class="clearfix"></div>
    
    <div style="width:150px;height:150px;">
        <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
        <img src="<?= $model->getImages(); ?>" class="previewCategoryImage img-thumbnail"><br>
        <a class="btn btn-sm btn-success addCategoryImage"style="width:100%" >Change image</a><br><br>
    </div>

    
    <div class="well">
        <?php foreach ($modelAttributes as $attribute) { ?>
            <div>
                <?php $value = 0; ?>
                <?php if ($modelAttributesCategory) { ?>
                    <?php $value = array_key_exists($attribute->id, $modelAttributesCategory) ? 1 : 0; ?>
                    <?php $filter = array_key_exists($attribute->id, $modelAttributesCategory) ? $modelAttributesCategory[$attribute->id]->filter : CategoriesAttributes::FILTER_TYPE_NO; ?>
                    <?php $filterType = array_key_exists($attribute->id, $modelAttributesCategory) ? ($modelAttributesCategory[$attribute->id]->filter_type !== null ? $modelAttributesCategory[$attribute->id]->filter_type : CategoriesAttributes::FILTER_TYPE_LEFT) : CategoriesAttributes::FILTER_TYPE_LEFT; ?>
                <?php } ?>
                <div class="col-xs-6">
                    <?= CheckboxX::widget([
                        'name' => 'Attribute[' . $attribute->id . ']',
                        'value' => $value,
                        'options' => [
                            'class' => ''
                        ],
                        'pluginOptions' => [
                            'threeState' => false
                        ]
                    ]); ?>
                    <?= $attribute->name; ?>
                </div>
                <div class="col-xs-3">
                    <label>Filter</label>
                    <?= Html::dropDownList('Filter[' . $attribute->id . ']', $filter, CategoriesAttributes::$filterArray, ['class' => 'form-control'])?>
                </div>
                <div class="col-xs-3">
                    <label>Filter type</label>
                    <?= Html::dropDownList('FilterType[' . $attribute->id . ']', $filterType, CategoriesAttributes::$filterTypes, ['class' => 'form-control'])?>
                </div>
            </div>
            <hr>
            <div class="clearfix"></div>
        <?php } ?>
    </div>

    <?php if (!$model->isNewRecord) { ?>
        <div class="row">
            <h2>Sponsored links</h2>
            <?php for ($i = 1; $i <= Categories::SPONSORED_COUNT; $i++) { ?>
                <?php
                    if ($productS = ProductsSponsored::findOne(['category_id' => $model->id, 'sort' => $i])) {
                        $model->sponsored = [];
                        $model->sponsored[$i] = $productS->product_id;
                    }
                ?>
                <div class="col-sm-3">
                    <?= Select2::widget([
                        'model' => $model,
//                        'value' => 8,
                        'attribute' => 'sponsored[' . $i . ']',
                        'data' => $productArray,
                        'options' => ['placeholder' => 'Select a state ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
            <?php } ?>
        </div>
        <hr>
    <?php } ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
