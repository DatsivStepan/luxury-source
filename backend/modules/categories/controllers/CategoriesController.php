<?php

namespace backend\modules\categories\controllers;

use Yii;
use common\models\Categories;
use backend\modules\categories\models\SearchCategories;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\CategoriesAttributes;
use yii\helpers\ArrayHelper;
use common\models\Attributes;
use common\models\ProductsSponsored;
/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchCategories();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Categories();
        $modelAttributes = Attributes::find()->all();
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if (Yii::$app->request->post('Attribute')) {
                    CategoriesAttributes::saveCategoryAttributes(Yii::$app->request->post('Attribute'), Yii::$app->request->post('Filter'), $model->id, Yii::$app->request->post('FilterType'));
                }
            }
            return $this->redirect(['index']);
        }
        
        return $this->render('create', [
            'model' => $model,
            'modelAttributes' => $modelAttributes,
        ]);
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelAttributes = Attributes::find()->all();
        $modelAttributesCategory = 
                ArrayHelper::index(CategoriesAttributes::findAll(['category_id' => $model->id]), 'attribute_id');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
               ProductsSponsored::saveSponsoredLinks($model->id, $model->sponsored);
                if (Yii::$app->request->post('Attribute')) {
                    CategoriesAttributes::saveCategoryAttributes(Yii::$app->request->post('Attribute'), Yii::$app->request->post('Filter'), $model->id, Yii::$app->request->post('FilterType'));
                }
                return $this->redirect(['index']);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
            'modelAttributes' => $modelAttributes,
            'modelAttributesCategory' => $modelAttributesCategory,
        ]);
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSavecategoryphoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/category/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.png';  //5
            move_uploaded_file($tempFile,$targetFile); //6
            return 'admin/images/category/'.\Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.png'; //5
        }
    }
    
    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
