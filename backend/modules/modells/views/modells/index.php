<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\brands\models\BrandsCategories */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Models');
$this->params['breadcrumbs'][] = $this->title;

?>



    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        
        <div class="modells-index">

            <p>
                <?= Html::a(Yii::t('app', 'Create Modells'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    [
                        'attribute' => 'brand_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->brandModel ? $model->brandModel->name : null;
                        }
                    ],
                    [
                        'label' => 'Logo',
                        'attribute' => 'img_src',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::tag('img', '', [
                                'src' => $model->getImages(),
                                'class' => 'img-thumbnail',
                                'style' => 'width:100px;background-color:black;',
                            ]);
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'delete' => function ($url, $model) {
                                return Html::a(
                                    '<i class="glyphicon glyphicon-trash"></i>',
                                    ['delete', 'id' => $model->id],
                                    [
                                        'title' => 'delete',
                                        'data-pjax' => 0,
                                        'aria-label' => "Delete",
                                        'data-method' => "post",
                                        'data-confirm' => "Are you sure you want to delete this item?", 
                                        'class' => 'btn btn-xs btn-danger'
                                    ]
                                );
                            },
                            'update' => function ($url, $model) {
                                return Html::a(
                                    '<i class="glyphicon glyphicon-pencil"></i>',
                                    ['update', 'id' => $model->id],
                                    ['data-pjax' => 0, 'class' => 'btn btn-xs btn-primary']
                                );
                            }
                        ],
                    ],
                ],
            ]); ?>

        </div>

    </section>