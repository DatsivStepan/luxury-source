<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use froala\froalaeditor\FroalaEditorWidget;
use kartik\select2\Select2;
use common\models\PagesCategory;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    

    <?= $form->field($model, 'category_id')->dropDownList(PagesCategory::getAllInArrayMap(), ['prompt' => 'Select category for pages']); ?>

    <div class="well">
        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
        <h3>OR</h3>
        <?= FroalaEditorWidget::widget([
            'model' => $model,
            'attribute' => 'content',
            'options' => [
                // html attributes
                'id'=>'content'
            ],
            'clientOptions' => [
                'toolbarInline' => false,
                'theme' => 'royal', //optional: dark, red, gray, royal
                'language' => 'en_gb', // optional: ar, bs, cs, da, de, en_ca, en_gb, en_us ...,
                'toolbarButtons' => ['fullscreen', 'bold', 'italic', 'underline', 'align', 'formatOL', 'formatUL', '|', 'paragraphFormat', 'insertImage', 'html'],
                'imageUploadParam' => 'file',
                'imageUploadURL' => \yii\helpers\Url::to(['/pages/pages/save-image'])
            ]
        ]); ?>
    </div>

    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
