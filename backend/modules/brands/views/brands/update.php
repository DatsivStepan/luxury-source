<?php

use yii\helpers\Html,
    yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\Brands */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Brands',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        
        <div class="brands-update">

            <?= $this->render('_form', [
                'model' => $model,
                'arrayCategories' => $arrayCategories,
            ]) ?>

        </div>

    </section>