<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%categories_attributes}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $attribute_id
 * @property string $created_at
 * @property string $updated_at
 */
class CategoriesAttributes extends \yii\db\ActiveRecord
{
    const FILTER_TYPE_YES = 1;
    const FILTER_TYPE_NO = 2;
    
    const FILTER_TYPE_UP = 1;
    const FILTER_TYPE_LEFT = 0;

    public static $filterArray = [
        self::FILTER_TYPE_YES => 'Yes',
        self::FILTER_TYPE_NO => 'No'
    ];

    public static $filterTypes = [
        self::FILTER_TYPE_UP => 'UP PAGE',
        self::FILTER_TYPE_LEFT => 'LEFT SIDEBAR'
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories_attributes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'attribute_id', 'filter', 'filter_type'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'attribute_id' => Yii::t('app', 'Attribute ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public static function saveAttributesCategory($data = [], $attribute_id)
    {
        self::deleteAll(['attribute_id' => $attribute_id]);
        foreach ($data as $category_id => $value) {
            if ($value == 1) {
                $model = new CategoriesAttributes;
                $model->attribute_id = $attribute_id;
                $model->category_id = $category_id;
                $model->save();
            }
        }
    }
    
    public static function saveCategoryAttributes($data = [], $filter = [], $category_id, $filterType = [])
    {
        self::deleteAll(['category_id' => $category_id]);
        foreach ($data as $attribute_id => $value) {
            if ($value == 1) {
                $model = new CategoriesAttributes;
                $model->attribute_id = $attribute_id;
                $model->category_id = $category_id;
                $model->filter = array_key_exists($attribute_id, $filter) ? $filter[$attribute_id] : 1;
                $model->filter_type = array_key_exists($attribute_id, $filterType) ? $filterType[$attribute_id] : 0;
                $model->save();
            }
        }
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getAttributesModel()
    {
        return $this->hasOne(Attributes::className(), ['id' => 'attribute_id']);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getCategoriesModel()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }
    
}
