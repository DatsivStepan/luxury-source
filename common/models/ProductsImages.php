<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%products_images}}".
 *
 * @property integer $id
 * @property string $img_src
 * @property integer $product_id
 * @property string $created_at
 */
class ProductsImages extends \yii\db\ActiveRecord
{
    const TYPE_MAIN = 1;
    const TYPE_OTHER = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products_images}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'type', 'sort'], 'integer'],
            [['created_at'], 'safe'],
            [['img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'img_src' => Yii::t('app', 'Img Src'),
            'product_id' => Yii::t('app', 'Product ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
    
    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }
    
    public static function saveProductImages($images, $sort, $product_id)
    {
        self::deleteAll(['product_id' => $product_id]);
        foreach ($images as $key => $value) {
            $model = new ProductsImages();
            $model->sort = array_key_exists($key, $sort) ? $sort[$key] : 0;
            $model->product_id = $product_id;
            $model->img_src = $value;
            $model->save();
        }
    }
    
}
