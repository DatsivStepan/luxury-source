<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%products_attributes}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $attribute_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductsAttributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products_attributes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'attribute_id'], 'integer'],
            [['value', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'attribute_id' => Yii::t('app', 'Attribute ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getAttributeModel()
    {
        return $this->hasOne(Attributes::className(), ['id' => 'attribute_id']);
    }
    
    public static function saveProductAttributes($data = [], $product_id)
    {
        self::deleteAll(['product_id' => $product_id]);
        foreach ($data as $attribute_id => $value) {
            if(is_array($value)){
                $value = json_encode($value);
            }
            $model = new ProductsAttributes;
            $model->attribute_id = $attribute_id;
            $model->product_id = $product_id;
            $model->value = $value;
            $model->save();
        }
    }
}
