<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%brands}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property string $img_src
 * @property string $created_at
 * @property string $updated_at
 */
class Brands extends \yii\db\ActiveRecord
{
    public $categories;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%brands}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'categories'], 'safe'],
            [['name', 'img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'category_id' => Yii::t('app', 'Category'),
            'img_src' => Yii::t('app', 'Img Src'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->categories = \yii\helpers\ArrayHelper::getColumn(CategoriesBrands::getAllByCategory($this->id), 'category_id');
    }
    
    public function getLink()
    {
        return '/brands/' . $this->id; 
    }
    
    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }
    
    public function getChoiseCategory()
    {
        return \yii\helpers\ArrayHelper::getColumn(CategoriesBrands::getAllByCategory($this->id), 'category_id');
    }
    
    public static function getAllArrayMapByCategory($categoryId = null)
    {
        $query = self::find()
            ->alias('b')
            ->leftJoin(CategoriesBrands::tableName() . ' cb', 'cb.brand_id = b.id');

        if ($categoryId) {
            $query->where(['cb.category_id' => $categoryId]);
        }

        return \yii\helpers\ArrayHelper::map($query->all(), 'id', 'name');
    }
    
    public static function getAllInMapArray($categoryId = null)
    {
        return \yii\helpers\ArrayHelper::map(self::getAll(), 'id', 'name');
    }
    
    public static function getAll($categoryId = null)
    {
        $query = self::find();
        if ($categoryId) {
            $query->where(['id' => \yii\helpers\ArrayHelper::getColumn(CategoriesBrands::getAllBrandsByCategory($categoryId), 'brand_id')]);
        }
        return $query->all();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getCategoryModel()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }
}
