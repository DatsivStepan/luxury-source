<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%products}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property integer $brand_id
 * @property integer $modells_id
 * @property integer $shop_id
 * @property string $price
 * @property string $price_retail
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class Products extends \yii\db\ActiveRecord
{
    public $images;
    public $imagesSort;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'category_id', 'name'], 'required'],
            [['category_id', 'brand_id', 'modells_id', 'shop_id', 'parent_id'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at','images', 'imagesSort'], 'safe'],
            [['name', 'img_src'], 'string', 'max' => 255],
            [['price', 'price_retail'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'parent_id' => Yii::t('app', 'Parent'),
            'category_id' => Yii::t('app', 'Category'),
            'brand_id' => Yii::t('app', 'Brand'),
            'modells_id' => Yii::t('app', 'Models'),
            'shop_id' => Yii::t('app', 'Shop'),
            'price' => Yii::t('app', 'Price'),
            'price_retail' => Yii::t('app', 'Price Retail'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public function getShopName()
    {
        return $this->shopModel ? $this->shopModel->name : '';
    }

    public function getBrandName()
    {
        return $this->brandModel ? $this->brandModel->name : '';
    }

    public function getModelsName()
    {
        return $this->modellsModel ? $this->modellsModel->name : '';
    }

    public function getProductCode()
    {
        return $this->id;
    }

    public function getParentProductCode()
    {
        return $this->parentModel ? $this->parentModel->id . ' LN' : $this->getProductCode();
    }

    public function getPrice()
    {
        return '£' . ($this->price ? $this->price : 0);
    }
    
    public function getPriceRetail()
    {
        return '£' . ($this->price_retail ? $this->price_retail : 0);
    }

    public function getLink()
    {
//        if (($this->parent_id == 0 || $this->parent_id == null) && ($this->shop_id == 0 || $this->shop_id == null)) {
//            return '/category/compare/' . $this->id;
//        } else {
            return '/product/view/' . $this->id;
//        }
    }

    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getCategoryModel()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getBrandModel()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getModellsModel()
    {
        return $this->hasOne(Modells::className(), ['id' => 'modells_id']);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getShopModel()
    {
        return $this->hasOne(Shops::className(), ['id' => 'shop_id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getParentModel()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductsAttributesModel()
    {
        return $this->hasMany(ProductsAttributes::className(), ['product_id' => 'id']);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public static function getAllAdmin($categoryId = null, $limit = null)
    {
        $query = self::find()
                ->where([
                    "OR",
                    ['parent_id' => 0],
                    ['parent_id' => null],
                ])
                ->andWhere([
                    "OR",
                    ['shop_id' => 0],
                    ['shop_id' => null],
                ]);
        if ($categoryId) {
            $query->andWhere(['category_id' => $categoryId]);
        }
        if ($limit) {
            $query->limit($limit);
        }
        
        return $query->all();
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductsImagesModel()
    {
        return $this->hasMany(ProductsImages::className(), ['product_id' => 'id'])
                ->orderBy(['sort' => SORT_ASC]);
    }
    
    public function getProductsAttributesForView()
    {
        $resArray = [];
        if ($this->productsAttributesModel) {
            foreach ($this->productsAttributesModel as $productAttribute) {
                $attribute = $productAttribute->attributeModel;
                $value = '';
                switch ($attribute->type) {
                    case Attributes::TYPE_TEXT:
                        $value = $productAttribute->value;
                        break;
                    case Attributes::TYPE_SELECT:
                    case Attributes::TYPE_RADIO:
                        $value = $attribute->getValueById($productAttribute->value);
                        break;
                    case Attributes::TYPE_CHECKBOX:
                        foreach (json_decode($productAttribute->value) as $key => $av_id) {
                            $value .= ($key > 0 ? '; ' : '') . $attribute->getValueById($av_id);
                        }
                }
                $resArray[] = [
                    'name' => $attribute->name,
                    'value' => $value
                ];
            }
        }
        return $resArray;
    }
}
