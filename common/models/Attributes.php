<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%attributes}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $created_at
 * @property string $updated_at
 */
class Attributes extends \yii\db\ActiveRecord
{
    const TYPE_RADIO = 1;
    const TYPE_SELECT = 2;
    const TYPE_CHECKBOX = 3;
    const TYPE_TEXT = 4;
    
    public static $typeArray  = [
        self::TYPE_TEXT => 'text',
        self::TYPE_RADIO => 'radio',
        self::TYPE_SELECT => 'select',
        self::TYPE_CHECKBOX => 'checkbox',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attributes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public function getValueById($id)
    {
        $model = AttributesValue::find()->where(['attribute_id' => $this->id, 'id' => $id])->one();
        return $model ? $model->value : '';
    }
    
    
    public function getAttributesValueArray()
    {
        return $this->attributesValueModel ? ArrayHelper::map($this->attributesValueModel, 'id', 'value') : '';
    }
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getAttributesValueModel()
    {
        return $this->hasMany(AttributesValue::className(), ['attribute_id' => 'id']);
    }
}
