<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%categories_brands}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $brand_id
 * @property string $created_at
 * @property string $updated_at
 */
class CategoriesBrands extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories_brands}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'brand_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'brand_id' => Yii::t('app', 'Brand ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public static function getAllByCategory($brand_id)
    {
        return self::findAll(['brand_id' => $brand_id]);
    }
    
    public static function getAllBrandsByCategory($category_id)
    {
        return self::findAll(['category_id' => $category_id]);
    }
    
    public static function saveCategoriesBrands($data = [], $brand_id)
    {
        self::deleteAll(['brand_id' => $brand_id]);
        foreach ($data as $key => $category_id) {
            $model = new self;
            $model->category_id = $category_id;
            $model->brand_id = $brand_id;
            $model->save();
        }
    }
}
