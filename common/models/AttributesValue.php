<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%attributes_value}}".
 *
 * @property int $id
 * @property int $attribute_id
 * @property string $value
 * @property string $created_at
 * @property string $updated_at
 */
class AttributesValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attributes_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'sort'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'attribute_id' => Yii::t('app', 'Attribute ID'),
            'value' => Yii::t('app', 'Value'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public static function saveAttributesOptions($data = [], $attribute_id)
    {
        self::deleteAll(['attribute_id' => $attribute_id]);
        foreach ($data['value'] as $key => $value) {
            $model = new AttributesValue;
            $model->attribute_id = $attribute_id;
            $model->value = $value;
            $model->sort = $data['sort'][$key];
            $model->save();
        }
    }
    
}
