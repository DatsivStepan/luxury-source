<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%products_sponsored}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $product_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductsSponsored extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products_sponsored}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'product_id', 'sort'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public static function getAllSponsoredByCategory($category_id, $limit)
    {
        $query = self::find()->where(['category_id' => $category_id]);
        if ($limit) {
            $query->limit((int)$limit);
        }
        $productId = [];
        foreach ($query->orderBy(['sort' => SORT_ASC])->all() as $categProd) {
            $productId[] = $categProd->product_id;
        }
        
        return Products::findAll($productId);
    }

    public static function saveSponsoredLinks($category_id, $productsArray)
    {
        self::deleteAll(['category_id' => $category_id]);
        foreach ($productsArray as $key => $product_id) {
            if ($product_id) {
                $model = new self;
                $model->category_id = $category_id;
                $model->product_id = $product_id;
                $model->sort = $key;
                $model->save();
            }
        }
        return true;
    }
}
