<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property string $sort
 * @property string $date_create
 * @property string $date_update
 * @property string $slug
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['category_id'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['name', 'sort', 'slug', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'content' => Yii::t('app', 'Content'),
            'sort' => Yii::t('app', 'Sort'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'slug' => Yii::t('app', 'Slug'),
        ];
    }
    
    public function getLink()
    {
        return $this->url ? $this->url : '/pages/' . ($this->slug ? $this->slug : $this->id);
    }
    
    
    public static function getAllByCategoryId($id)
    {
        return self::find()->where(['category_id' => $id])->orderBy(['sort' => SORT_ASC])->all();
    }
    
    public static function getAllFromFooter()
    {
        $resArray = [];
        foreach (PagesCategory::getAllInArrayMap() as $category_id => $categoryName) {
            $resArray[] = [
                'name' => $categoryName,
                'items' => self::getAllByCategoryId($category_id),
            ];
        }
        
        return $resArray;
    }
    
    public function getPageCategoryModel()
    {
        return $this->hasOne(PagesCategory::className(), ['id' => 'category_id']);
    }
}
