<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $img_src
 * @property string $text_slide_1
 * @property string $text_slide_2
 * @property string $created_at
 * @property string $updated_at
 */
class Categories extends \yii\db\ActiveRecord
{
    const SPONSORED_COUNT = 4;
    public $sponsored;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'sponsored'], 'safe'],
            [['name', 'img_src', 'text_slide_1', 'text_slide_2'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'img_src' => Yii::t('app', 'Img Src'),
            'text_slide_1' => Yii::t('app', 'Text slide 1'),
            'text_slide_2' => Yii::t('app', 'Text slide 2'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }

    public static function getAllInArrayMap()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
    
     /**
     * return yii\data\ActiveDataProvider
     */
    public function getCategoriesAttributeModel()
    {
        return $this->hasMany(CategoriesAttributes::className(), ['category_id' => 'id'])
                ->andOnCondition(['filter' => '1']);
    }
    
    
    public function getAttributeForFilter($limit, $filterType)
    {
        $filterType = $filterType ? $filterType : CategoriesAttributes::FILTER_TYPE_LEFT;
        $resultArray = [];
        if ($categoryAttribute = CategoriesAttributes::findAll(['category_id' => $this>id, 'filter' => CategoriesAttributes::FILTER_TYPE_YES, 'filter_type' => $filterType])) {
            $k = 0;
            foreach ($categoryAttribute as $categoryAttribute) { 
                $attribute = $categoryAttribute->attributesModel;
                if ($k < $limit) { 
                   if (($attribute->type > 0) && ($attribute->type < Attributes::TYPE_TEXT)) { 
                        $resultArray[] = $attribute;
                        $k++; 
                    } 
                 } 
             } 
        }
             
        return $resultArray;
    }
}
