<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%modells}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $brand_id
 * @property string $img_src
 * @property string $created_at
 * @property string $updated_at
 */
class Modells extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%modells}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['brand_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'brand_id' => Yii::t('app', 'Brand ID'),
            'img_src' => Yii::t('app', 'Img Src'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
//    public function getLink()
//    {
//        return '/brands/' . $this->id; 
//    }
    
    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }
    
    public static function getAll($brandId = null)
    {
        $query = self::find();
        if ($brandId) {
            $query->where(['brand_id' => $brandId]);
        }
        return $query->all();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getBrandModel()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }
}
